

var nfa = (function () {
    var nfas = [];
    var bufferedEvents;
    var buffer; //Buffer up to maxBuffer events
    var maxBuffer = 100; //max number of events buffered in buffer
    
    var timeoutTrigger = false; //This variable is set to true by the timeout function when the interval of a time-specific event has potentially ended. 
                                // This will result in special behaviour when the next event is matched (e.g. the state may be reversed)
    var timeInformation = [];
    
    var isParallelNfa = 1;
    var numberOfUsers = 4; //Stores the number of users for the parallel automaton
    var initializeParallelNfa = function(newNfa){
        newNfa.currentStates = [];
        for(var i = 0; i < numberOfUsers; i++){
            newNfa.currentStates[i] = { at : "start", matched : {} }; //State consists of progress in "at" and matched events in "matched". 
        }
    }
    
    
    //TODO: This check for user is bad
    var hasCorrectUser = function(event, userNumber){
        var ret = false;
        try{
            ret = event.User === userNumber;
        }catch (e) {
            ret = false;
        }
        return ret;
    }
    
    var deleteNfa = function(nfaIndex){
        if(nfaIndex && nfas[nfaIndex]){
            delete nfas[nfaIndex];
        } else {
            console.log("Tried removing nonexisting nfa at index: " + nfaIndex);
        }
    }
    
    //Revert the state of timing relevant events
    var revertIfStayed = function(nfaIndex, userIndex, original, partner) {
        console.log("NFAIndex: " + nfaIndex);
        console.log("UserIndex: " + userIndex);
        console.log("ori: " + original);
        console.log("partner: " + partner);
        var current = nfas[nfaIndex].currentState[userIndex].at;
        if (current == partner){ //TODO: Transitive closure...
            
        } else {
            current = original;
        }
    }
    
    var activateTimeoutTrigger = function(){
        timeoutTrigger = true; 
    }
    
    
    //Checks if currentState is in transitive closure of firstName (first arg)
    var isInTransitiveClosure = function(nfaLabel, user, firstName) {
      if(!nfas[nfaLabel][firstName]){
          //Error
          return;
      }
      
      var currentName = firstName;
      var secondName = nfas[nfaLabel].currentStates[user].at;
      if(firstName === secondName){
          return true; 
      }
      while(nfas[nfaLabel][currentName].nextState){
          if(nfas[nfaLabel][currentName].nextState === secondName){
              return true;
          }
          currentName = nfas[nfaLabel][currentName].nextState;
      }
      return false;
  }
    // Cleans up after reaching final state
    var cleanupFinal = function(currentNfa, currentUser, currentState){
        //Revert state, array of matched events
        currentState.at = "start";
        currentState.matched = {};
        //Delete all timeSpecific info from the timeInformation array
        for(timeInfo in timeInformation){
            var currentInfo = timeInformation[timeInfo];
            if(currentInfo.nfaLabel === currentNfa && currentInfo.user === currentUser){
                delete timeInformation[timeInfo];
            }
        }
    }
    
    
    //tries to match event on nfa with user with the conditions of state
    var matchEvent = function (event, nfa, nfaLabel, user, state){
        //console.log(nfa); //DEBUG
        //console.log(state); //DEBUG
        myNextStateLabel = state.at; //Label of next state to be matched
        myNextState = nfa[myNextStateLabel]; //Next State to be matched, contains condition
        //console.log(myNextStateLabel); //DEBUG
        //console.log(myNextState); // DEBUG
        var args = currentState.matched; //
        args[myNextStateLabel] = event;
        //console.log("args"); // DEBUG
        //console.log(args); // DEBUG

        if(!myNextState.isUserSpecific || hasCorrectUser(event, user + 1) ){
            console.log("a");
            //set all other states to unspecific, only first event has to be correct
            /*
            var nextStateRowLabel = myNextState.nextState;
            var nextStateRow;
            while(nextStateRowLabel){
                nextStateRow = currentNFA[nextStateRowLabel];
                nextStateRow.isUserSpecific = false;
                nextStateRowLabel = nextStateRow.nextState;                            
            }
            */
            //now check if the array of conditions all return true
           var isConditionTrue = true; // Set to true to match events without conditions
           for (var j = 0; j < myNextState.condition.length; j++){
              isConditionTrue &= myNextState.condition[j](args);
           }
           // } && myNextState.condition(args)){ //Check for user i + 1, since user numbers start with 1? TODO
            if(isConditionTrue){ //go to next State
                console.log("Event matched!"); //DEBUG
                state.at = myNextStateLabel;
                if(myNextState.final){
                    console.log("Reached a final state"); //DEBUG
                    nfa.callback(); //TODO: Parameters? Maybe clone matched and return that
                    cleanupFinal(currentNFALabel, i, currentState);
                    /* Deprecated, use cleanupFinal instead
                        currentState.at = "start";
                        currentState.matched = {};
                    */
                } else { //No final state
                    if(myNextState.times){ //check if this event has some time constraints
                        for(pairs in myNextState.times){
                            var currentPair = myNextState.times[pairs];
                            var time = currentPair.time;
                            var partner = currentPair.partner;
                            var hasReached = false; //TEST, NOT FINAL
                            //var user =  i; //JSON.parse(JSON.stringify(i));
                            //var currentNFA = currentNFALabel; //JSON.parse(JSON.stringify(currentNFALabel));
                            var startTime = (new Date()).getTime(); 
                            var endTime = startTime + time; 
                            
                            
                            
                            //TODO
                            var currentStateLabel = nfa[state.at].previousState;                         
                            
                            
                            
                            
                            var timeObject = {"endTime" : endTime, "oldAt" : currentStateLabel, "nfaLabel": nfaLabel, "user" : user , "partner": partner};                            
                            
                            //Delete old timeObject if it exists
                            for(timeInfo in timeInformation){
                                var currentTimeInfo = timeInformation[timeInfo];
                                if(currentTimeInfo.oldAt === timeObject.oldAt 
                                   && currentTimeInfo.nfaLabel === timeObject.nfaLabel
                                  && currentTimeInfo.user === timeObject.user 
                                  && currentTimeInfo.partner === timeObject.partner) {
                                    delete currentTimeInfo;
                                    console.log("Deleting old timeout"); //Debug? 
                                }
                            }
                            
                            //THEN push new timeObject
                            timeInformation.push(timeObject);
                            
                            
                            
                            
                            //console.log(timeInformation); //DEBUG
                            window.setTimeout(function(){
                                /*revertIfStayed(
                                    JSON.parse(JSON.stringify(currentNFALabel)), 
                                    JSON.parse(JSON.stringify(i)), 
                                    JSON.parse(JSON.stringify(myNextStateLabel)), 
                                    JSON.parse(JSON.stringify(partner)));
                                    */
                                activateTimeoutTrigger();
                                /*
                                if(!(currentNFA.at === partner || hasReached) ){ // We weren't able to match the event, revert!
                                    console.log("Reverting back!")
                                    currentState.at = myNextState.previousState;
                                    //TODO: Set all matched to null or undefined
                                    console.log(currentState);
                                    console.log(myNextState.times[0].partner);
                                }
                                */
                            }, time);
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    
    return {
        savedNfas : nfas,
        addNfa : function (newNfa){ //add the newly created nfa to the array of nfas
            if(isParallelNfa === 1){ //this happens only for parallel nfas
                initializeParallelNfa(newNfa);
                
            }else{ //this is not supported yet
                //newNfa.currentStates = [];
                //newNfa.currentStates[0] = { at : "start", matched : {} }; //State consists of progress in "at" and matched events in "matched". 
                
            }
            nfas[nfas.length] = newNfa;
        },
        /* Nondeterministic steps with buffering etc, unsupported 
        doStepOld : function(event){
            for(currentNFALabel in nfas){ //TODO: Check for nextState == undefined and such...
                currentNFA = nfas[currentNFALabel];
                for(currentStateLabel in currentNFA.currentStates){
                    currentState = currentNFA.currentStates[currentStateLabel];
                    myNextStateLabel = currentNFA[currentState.at].nextState;
                    myNextState = currentNFA[myNextStateLabel];
                    //DEBUG on
                    console.log("Current NFA");
                    console.log(currentNFA);
                    console.log("Current State");
                    console.log(currentState);
                    console.log("Next State");
                    console.log(myNextState);
                    //DEBUG off
                    //Match event to condition:
                    if(myNextState.condition(event)){
                        //If there is still room in the buffer, we create a new state and change that
                        if(currentNFA.currentStates.length < maxBuffer){
                            currentNFA.currentStates[currentNFA.currentStates.length] = JSON.parse(JSON.stringify(currentState));
                            currentState = currentNFA.currentStates[currentNFA.currentStates.length - 1];
                        }
                        //Next: Save event in matched TODO
                        currentState.matched[myNextStateLabel] = event;
                        //And proceed along the transition
                        currentState.at = myNextStateLabel;
                        //Check for final state TODO HERE
                        if(currentNFA[myNextStateLabel].final){
                            console.log("reached final state!");
                            currentNFA.callback(); // TODO: Parameters? 
                        }
                    }
                }
            }
        },
        */
        /**
         * DoStep for parallel automatons, using an object for each player to store their progress
         **/
        doStep : function(event){
            
            //Handle time- specific event if timeoutTrigger is activated
            if(timeoutTrigger){
                console.log("Timeout Trigger was set") // DEBUG
                var timeNow = (new Date()).getTime();
                //check which (possibly several) timeouts have ended
                for(timeInfo in timeInformation){
                    var currentEvent = timeInformation[timeInfo];
                    if(!currentEvent){
                        continue;
                    }
                    //is the time for this event over? 
                    if(currentEvent.endTime <= timeNow){
                        //revert state if partner was not found
                        if(!isInTransitiveClosure(currentEvent.nfaLabel, currentEvent.user, currentEvent.partner)) { //TODO: Check
                            nfas[currentEvent.nfaLabel].currentStates[currentEvent.user].at = currentEvent.oldAt;
                            console.log("Reverting State");
                        }
                        delete timeInformation[timeInfo];
                    }
                    
                }
                timeoutTrigger = false;
            }
            
            //Iterate through all nfas and try matching the new event
            for(currentNFALabel in nfas){
                currentNFA = nfas[currentNFALabel];
                //Check if currentNfa was deleted, continue if this is the case
                if(!currentNFA){
                    console.log("Nfa at: " + currentNFALabel + " is empty!");
                    continue;
                }
                
                //for(currentStateLabel in currentNFA.currentStates){
                for(var i = 0; i < numberOfUsers; i++){ //Iterate users, current user number saved in i
                    currentState = currentNFA.currentStates[i]; //Remembers the state this user is in, contains all the conditions, nextState etc
                    currentStateLabel = currentState.at;
                    myNextStateLabel = currentNFA[currentState.at].nextState; //Label of next state to be matched
                    myNextState = currentNFA[myNextStateLabel]; //Next State to be matched, contains condition
                    var args = currentState.matched; //
                    args[myNextStateLabel] = event;
                    //console.log("args"); // DEBUG
                    //console.log(args); // DEBUG
                    
                    if(!myNextState.isUserSpecific || hasCorrectUser(event, i + 1) ){
                        //set all other states to unspecific, only first event has to be correct
                        var nextStateRowLabel = myNextState.nextState;
                        var nextStateRow;
                        while(nextStateRowLabel){
                            nextStateRow = currentNFA[nextStateRowLabel];
                            nextStateRow.isUserSpecific = false;
                            nextStateRowLabel = nextStateRow.nextState;                            
                        }
                        //now check if the array of conditions all return true
                        var isConditionTrue = true; // Set to true to match events without conditions
                       for (var j = 0; j < myNextState.condition.length;j++){
                          isConditionTrue &= myNextState.condition[j](args);
                       }
                       // } && myNextState.condition(args)){ //Check for user i + 1, since user numbers start with 1? TODO
                        if(isConditionTrue){ //go to next State
                            console.log("Event matched!"); //DEBUG
                            currentState.at = myNextStateLabel;
                            if(myNextState.final){
                                console.log("Reached a final state"); //DEBUG
                                currentNFA.callback(); //TODO: Parameters? Maybe clone matched and return that
                                cleanupFinal(currentNFALabel, i, currentState);
                                /* Deprecated, use cleanupFinal instead
                                    currentState.at = "start";
                                    currentState.matched = {};
                                */
                            } else { //No final state
                                
                                if(myNextState.times){ //check if this event has some time constraints
                                    for(pairs in myNextState.times){
                                        var currentPair = myNextState.times[pairs];
                                        var time = currentPair.time;
                                        var partner = currentPair.partner;
                                        var hasReached = false; //TEST, NOT FINAL
                                        var user =  i; //JSON.parse(JSON.stringify(i));
                                        //var currentNFA = currentNFALabel; //JSON.parse(JSON.stringify(currentNFALabel));
                                        var startTime = (new Date()).getTime(); 
                                        var endTime = startTime + time; 
                                        //console.log(currentStateLabel); //DEBUG ############
                                        var timeObject = {"endTime" : endTime, "oldAt" : currentStateLabel, "nfaLabel": currentNFALabel, "user" : i , "partner": partner};
                                        timeInformation.push(timeObject);
                                        //console.log(timeInformation); //DEBUG
                                        window.setTimeout(function(){
                                            /*revertIfStayed(
                                                JSON.parse(JSON.stringify(currentNFALabel)), 
                                                JSON.parse(JSON.stringify(i)), 
                                                JSON.parse(JSON.stringify(myNextStateLabel)), 
                                                JSON.parse(JSON.stringify(partner)));
                                                */
                                            activateTimeoutTrigger();
                                            /*
                                            if(!(currentNFA.at === partner || hasReached) ){ // We weren't able to match the event, revert!
                                                console.log("Reverting back!")
                                                currentState.at = myNextState.previousState;
                                                //TODO: Set all matched to null or undefined
                                                console.log(currentState);
                                                console.log(myNextState.times[0].partner);
                                            }
                                            */
                                        }, time);
                                    }
                                }
                            }
                        } else { //User matched but conditions failed
                            args[myNextStateLabel] = undefined; //Necessary? 
                            matchEvent(event, currentNFA, currentNFALabel, i, currentState);
                            
                        }
                    } else {
                        args[myNextStateLabel] = undefined; //Necessary? 
                        //Try matching current state again if it has a time limit
                        matchEvent(event, currentNFA, currentNFALabel, i, currentState);
                    }
                    //console.log(currentNFA); //DEBUG
                }
            }
        },
        setBufferSize(newSize){
            maxBuffer = newSize;
        },
        getBufferSize(){
            return maxBuffer;
        },
    }
})();



var parser = (function f() {

  var seqMatcher = /[Ss][Ee][Qq]/m; //Matches SEQ caseinsensitively allowing (unnecessarily for multiline matches). TODO: Use /i instead? 
  var tMatcher = /[Tt]/m;
  var whereMatcher = /[Ww][Hh][Ee][Rr][Ee]/m;
  var varMatcher = /[a-z][\w]*/m;
  var wordBracketMatcher = /[\w()]/m;
  var spaceMatcher = /[\s\n]/m;
  var wordMatcher = /\w/m;
  var commaMatcher = /,/m;
  var wordCommaBracketMatcher = /[\w,()]/m;
  var wordBracketEqualMatcher = /[\w=()]/m;
  var numberMatcher = /\d/m;
  var wordDotMatcher = /[\w.]/m;
  var varExtendedMatcher = /[a-z][\w.]*/m;
  var termMatcher = /[\w.+*=><\s\n\"'!]/m;
  var userMatcher = /\w*[\w.]*.User/i; //Matches exactly the user variable in an event
  var timeMatcher = /^\w*(.\w+)*.Time$/i;
  
    
  var localNfa; //Contains the NFA in its current state, as it is created. Used to add conditions in whereTranslator, ...
    
    
    
  /**
    Returns an array of tokens
  **/
  var lexer = function(input){
      var ret = [];
      var currentPos = 0;
      while(currentPos < input.length){
          var currentChar = input.charAt(currentPos);
          //Check for words
          if( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') ){
              var currentVariable = "";
              while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') || (currentChar === '.') || (currentChar >= '0' && currentChar <= '9') ){
                  currentVariable += currentChar;
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
            ret[ret.length] = {
                name : "Variable",
                attribute : currentVariable,
            }
          //Check for numbers
          } else if ( currentChar >= '0' && currentChar <= '9') {
          //} else if (!isNaN(currentChar)){
              var currentNumber = 0;
              while( currentChar >= '0' && currentChar <= '9'){
              //while(!isNaN(currentChar)){
                  currentNumber *= 10;
                  currentNumber += +currentChar;
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
              ret[ret.length] = {
                name : "Number",
                attribute : currentNumber,
              }
          }
          //Check for Strings (to compare member variables to)
          else if( currentChar === '"' || currentChar === '"'){
              var currentString = "";
              var bufferCurrentChar = currentChar; //remember what kind of quotation mark was used
              currentPos++; //but don't put it into the string
              currentChar = input.charAt(currentPos);
              //while read char != opening quotation mark
              while(currentChar !== bufferCurrentChar){
                  currentString += currentChar; //add char to string
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
              ret[ret.length] = {
                  name : "String",
                  attribute : currentString,
              }
              currentPos ++;
          }
          //Check for opening parenthesis
          else if (currentChar === '('){
              ret[ret.length] = {
                name : "Parentheses",
                attribute : '(',
              }
              currentPos ++;
          }
          //Next check for closing parenthesis
          else if (currentChar === ')'){
              ret[ret.length] = {
                name : "Parentheses",
                attribute : ')',
              }
              currentPos ++;
          }
          //Check for arithmetic symbols
          else if (currentChar === '+' || currentChar === '*' || currentChar === '/' || currentChar === '-' ){
              ret[ret.length] = {
                  name : "ArithOperator",
                  attribute : currentChar,
              }
              currentPos ++;
          }
          //Check for comparison operators
          else if(currentChar === '>' || currentChar === '<' || currentChar === '=' || currentChar === '!'){
              var nextChar = input.charAt(currentPos + 1);
              if(currentChar === '>' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : ">",
                  }
              }
              if(currentChar === '<' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "<",
                  }
              }
              if(currentChar === '>' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : ">=",
                  }
              }
              if(currentChar === '<' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "<=",
                  }
              }
              if(currentChar === '=' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "===",
                  }
              }
              if(currentChar === '=' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "===",
                  }
              }
              if(currentChar === '!' && nextChar === '='){
                  currentPos ++;
                  if(input.charAt(currentPos + 2) === '='){
                      currentPos ++;
                  }
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "!==",
                  }
              }
              
              currentPos ++;
          }
          //Check for the delimiting ','
          else if(currentChar === ','){
              ret[ret.length] = {
                  name : "Delimiter",
                  attribute : ",",
              }
          }
          else {
              currentPos ++;
          }
      }
      
      return ret;
      
  }
  /**
   ** Adds the corresponding function to the localNfa
   ** Checks for user specific events, adds special routine for this
   ** TODO: Check for time-specific events
   **/
  var whereTranslator = function(input){
      var ret = "return ";
      var occuringVarNames = [];
      var isUserSpecific = false; //Remember if some attribute was compared to the user attribute in the event
      var isTimeSpecific = false;
      for(var i = 0; i < input.length; i++){
          var currentToken = input[i];
          var currentName = currentToken.name;
          var currentAttribute = currentToken.attribute;
          //Special Case: Variables. 
          if(currentName === "Variable"){
              //Check if this var is userSpecific
              if(userMatcher.test(currentAttribute)){
                  isUserSpecific = true; //We found a comparison to user attribute, all occuring vars will have to be set to userSpecific
              } else if(timeMatcher.test(currentAttribute)){
                  isTimeSpecific = true;
                  // All Time logic here TODO: Recursive descent or something this
                  //Get the name of the first event
                  var firstEventName = currentAttribute;
                  //make sure next symbol is '+'
                  i ++;
                  var expectedPlus = input[i];
                  var expectedPlusToken = expectedPlus.name;
                  var expectedPlusAttribute = expectedPlus.attribute;
                  //followed by a number
                  i ++;
                  var expectedNumber = input[i];
                  var expectedNumberToken = expectedNumber.name;
                  var expectedNumberAttribute = expectedNumber.attribute;
                  //followed by '<'
                  i ++;
                  var expectedGeq = input[i];
                  var expectedGeqToken = expectedGeq.name;
                  var expectedGeqAttribute = expectedGeq.attribute;
                  //lastly another time attribute
                  i ++;
                  var expectedTime = input[i];
                  var expectedTimeToken = expectedTime.name;
                  var expectedTimeAttribute = expectedTime.attribute;
                  i++;
                  if(i < input.length){
                      var expectedDelim = input[i];
                      var expectedDelimToken = expectedDelim.name;
                      var expectedDelimAttribute = expectedDelim.attribute;
                      // console.log("i is now: " + i); //DEBUG
                  } else {
                      var expectedDelimToken = "Delimiter";
                      var expectedDelimAttribute = ",";
                      // console.log("i is now: " + i + "/" + input.length); //DEBUG
                  }
                  /*
                  //DEBUG
                  console.log(expectedPlusToken);
                  console.log(expectedNumberToken);
                  console.log(expectedGeqToken);
                  console.log(expectedTimeToken);
                  console.log(expectedDelimToken);
                  console.log(expectedPlusAttribute);
                  console.log(expectedNumberAttribute);
                  console.log(expectedGeqAttribute);
                  console.log(expectedTimeAttribute);
                  console.log(expectedDelimAttribute);
                  //DEBUG
                  */
                  if(expectedPlusToken !== "ArithOperator" || expectedPlusAttribute !== '+' 
                        || expectedNumberToken !== "Number" 
                        || expectedGeqToken !== "CompOperator" || expectedGeqAttribute !== ">"
                        || expectedTimeToken !== "Variable" || ! timeMatcher.test(expectedTimeAttribute)
                        || expectedDelimToken !== "Delimiter" || expectedDelimAttribute !== ","){
                      //ERROR HERE
                      console.log("Error parsing where part: Time specific events have to be of a certain form. See docs for more info.");
                      throw "Invalid Syntax";
                  } else {
                      //Add information to first state
                      var firstVarName = firstEventName.split(".")[0];
                      var secondVarName = expectedTimeAttribute.split(".")[0];
                      if(!localNfa[firstVarName]){
                          console.log("Trying to access undefined var name '" + buffer[0] + "' in WHERE part. Aborting!");
                          throw "Undefined var in WHERE";
                      }
                      localNfa[firstVarName].times = localNfa[firstVarName].times || [];
                      localNfa[firstVarName].times.push({time : expectedNumberAttribute, partner : secondVarName});
                  }
                  //end this loop
                  continue; //TODO: Randfall irgendwas mit max int langem input würde hier wohl kaputt gehen
                  
              }
              
              //handle var names with splitting on "." 
              var buffer = currentAttribute.split(".");
              var variable = ""; //This variable will contain the final var name
              occuringVarNames[occuringVarNames.length] = buffer[0];
              ///* ONLY COMMENTED OUT FOR TESTING PURPOSES! DEBUG
              if(!localNfa[buffer[0]]){
                  console.log("Trying to access undefined var name '" + buffer[0] + "' in WHERE part. Aborting!");
                  throw "Undefined var in WHERE";
              }
              //*/
              variable = 'args["' + buffer[0] + '"]'; //will only look for the first member
              //ret = ret + 'args["' + buffer[0] + '"]';
              for (var j = 1; j < buffer.length; j++){ //now we add all the other members
                  variable = variable + '["' + buffer[j] + '"]';
                  //ret = ret + '["' + buffer[j] + '"]';
              }
              ret = ret + variable; //add the variable to the end of our function
              //also check if we are trying to access undefined values, which would result in errors. Make this false instead
              ret = "try{" + variable + "}catch(e){console.log('Something went wrong while trying to access " + variable + "'); return false; }\n" + ret;
              
          }
          else if (currentName === "Number"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "Parentheses"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "ArithOperator"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "CompOperator"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "String"){
              ret = ret + '"' + currentAttribute + '"';
          }
          else if (currentName === "Delimiter") { //TODO: This is never executed right now (due to the way this function is called)
              
              //Handle user specific events
              if(isUserSpecific){
                  for(var m = 0; m < occuringVarNames.length; m++){
                      currentName[occuringVarNames[m]].isUserSpecific = true;
                  }
                  isUserSpecific = false;
              }
              
              //Find the correct state to add the condition to
              //by: calculating the transitive closure of all states
              //finding the var name that occurs the least in all closures?
              //or: --eliminating by finding one in the closure of the other?--
              for(var k = 0; k <= occuringVarNames.length -2; k++){
                  for(var l = k + 1; l <= occuringVarNames.length - 1; l++){
                      console.log("In for loop with k: " + k + " and l: " + l);
                        console.log(occuringVarNames);
                      if(occuringVarNames[k] !== occuringVarNames[l] && isInTransitiveClosure(occuringVarNames[k], occuringVarNames[l])){
                          occuringVarNames.splice(k,1);
                          console.log("In case with k: " + k + " and l: " + l);
                            console.log(occuringVarNames);
                          k--;
                          break;
                      } else if (occuringVarNames[k] !== occuringVarNames[l] && isInTransitiveClosure(occuringVarNames[l], occuringVarNames[k])){
                          occuringVarNames.splice(l,1);
                          l--;
                      }
                  }
              }
              
              //TODO: Try catch
              try{
                  //TODO: This does not work if x1.t < x2.t < x3.t we need IDs or sth. or have to add sth to ret
                /*if(isTimeSpecific){
                    localNfa[occuringVarNames[0]].isTimeSpecific = true; // only set the first var to is TimeSpecific
                }*/
                localNfa[occuringVarNames[0]].condition.push(Function("args",ret));
              }catch (e) {
                  console.log("Unable to parse where statement: \n'" + ret + "'. \nThis part will be ignored");
              }
              occuringVarNames = []; //Reset the occuring names for next clause
              console.log(ret); //DEBUG
          }
      } //end of for loop
      //save last where statement
      //TODO: Does this not require the same calculations as the non- last case? 
      /*
      var correctState = occuringVarNames[0];
      console.log(ret);
      localNfa[correctState].condition = Function("args",ret);
      */
      //copied from above
      //Handle user specific events
      if(isUserSpecific){
          for(var m = 0; m < occuringVarNames.length; m++){
              localNfa[occuringVarNames[m]].isUserSpecific = true;
          }
          isUserSpecific = false;
      }
      
      //Return if time specific (ret is empty)
      if(isTimeSpecific){
          isTimeSpecific = false; //Unnecessary
          return; 
      }
      
      //Find the correct state to add the condition to
      //by: calculating the transitive closure of all states
      //finding the var name that occurs the least in all closures?
      //or: --eliminating by finding one in the closure of the other?--
      for(var k = 0; k <= occuringVarNames.length -2; k++){
          for(var l = k + 1; l <= occuringVarNames.length - 1; l++){
              console.log("In for loop with k: " + k + " and l: " + l);
              console.log(occuringVarNames);
              if(occuringVarNames[k] !== occuringVarNames[l] && isInTransitiveClosure(occuringVarNames[k], occuringVarNames[l])){
                  occuringVarNames.splice(k,1);
                  console.log("In case with k: " + k + " and l: " + l);
                    console.log(occuringVarNames);
                  k--;
                  break;
              } else if (occuringVarNames[k] !== occuringVarNames[l] && isInTransitiveClosure(occuringVarNames[l], occuringVarNames[k])){
                  occuringVarNames.splice(l,1);
                  l--;
              }
          }
      }

      //TODO: Try catch
      try{
        localNfa[occuringVarNames[0]].condition.push(Function("args",ret));
      }catch (e) {
          console.log("Unable to parse where statement: \n'" + ret + "'. \nThis part will be ignored");
      }
      console.log(ret); //DEBUG
  }
  /*
   * as the name suggests, this function returns true if secondName is in the transitive closure of firstName
   */
  var isInTransitiveClosure = function(firstName, secondName) {
      if(!localNfa[firstName]){
          //Error
          return;
      }
      var currentName = firstName;
      while(localNfa[currentName].nextState){
          if(localNfa[currentName].nextState === secondName){
              return true;
          }
          currentName = localNfa[currentName].nextState;
      }
      return false;
  }

  
  
  /**
    Get the next word according to patMatch in text starting from position, ignore all patIgnore before patMatch
  **/
  var getNextWord = function (text, position, patMatch, patIgnore) {
      var ret = "";
      var inWord = false;
      var i = position;
      
      while (i < text.length && patIgnore.test(text.charAt(i))) {
          i++;
      }
      while (i < text.length && patMatch.test(text.charAt(i))){
          inWord = true;
          ret += text.charAt(i);
          i++;
      }
      if(inWord){
          return ret;
      }else{
          return undefined; // TODO
      }
      
  }
  
  /**
    Get the next char according to patMatch in text starting from position, ignore all patIgnore before patMatch
  **/
  var getNextChar = function (text, position, patMatch, patIgnore){
      var ret = "";
      var inWord = false;
      var i = position + 0;
      
      while(i < text.length && patIgnore.test(text.charAt(i))){
          i++;
      }
      if(i < text.length && patMatch.test(text.charAt(i))){
          inWord = true;
          ret += text.charAt(i);
          i++;
      }
      if(inWord){
          return ret;
      }else{
          return undefined; // TODO
      }
  }
  
  /**
    Returns the number of white spaces after position in text
  **/
  var whiteSpaceLength = function (text, position){
      var ret = 0;
      var i = position;
      while(i < text.length && /[\s\n]/m.test(text.charAt(i))){
          ret ++;
          i++;
      }
      return ret;
  }
  
  /**
    Create and output Generic Error message to console
  **/
  var parserError = function (mode, pattern, position){
      console.log("Error parsing " + mode + " part in \n'" 
            + pattern + "'\nat position: " + position
            +" (char: " + pattern.charAt(position) +").\nThe correct syntax is 'SEQ (var_1, ... ,var_n)' ");
  }
  
  /**
    Creates the condition functions corresponding to this where part
  **/
  var createCondition = function (currentNfa, text) {
    if(currentMatch = getNextWord(pattern, position, termMatcher, spaceMatcher)){
                    if(termMatcher.test(currentMatch)){ //Extract complete term, outsource this part
                        position += whiteSpaceLength(pattern, position);
                        position += currentMatch.length;
                        position += whiteSpaceLength(pattern, position);
                        nextChar = getNextChar(pattern, position, wordCommaBracketMatcher, spaceMatcher);
                        
                        console.log(currentMatch); //DEBUG
                        switch(nextChar){
                            case ",": position += /[^,]*,[\n\s]*/m.exec(pattern.slice(position))[0].length; break;
                            case ")": mode = "afterSeq"; 
                                position += /[^\)]*\)[\n\s]*/m.exec(pattern.slice(position))[0].length;
                                break;
                            default: 
                                break;
                        }
                            
                    } else {
                        parserError(mode, pattern, position);
                        mode = "Finished";
                    }
                } else {
                    console.log("Reached unexpected end of pattern!");
                    mode = "Finished";
                }  
  }
    
    
    
  return{

      /**
        This function takes a pattern and callback function as arguments and creates, as well as registers an NFA that matches the given pattern. 
      **/
    testLexer : lexer, 
    testTrans : whereTranslator,
    addPattern: function (pattern, callbackFunction){
        var mode = "Start";//parsing mode: Start -> in SEQ -> AfterSEQ -> inWhile -> afterSeq -> inT -> Finished
        var position = 0; //position in pattern
        var patternCpy = JSON.parse(JSON.stringify(pattern)); //create a copy to make sure the original is preserved (for error handling)
        var currentMatch; // current word as received by the "Lexer"
        localNfa = {start : {condition : []}, size : 1, callback : callbackFunction, }; //The NFA that will be given to the NFA handler in the end TODO: Is the condition is start necessary? 
        var lastState = "start"; //Remember the last state for transitions
      do{ //This do loop will go through the pattern and parse the next word according to the current mode
        console.log(mode); //DEBUG
        switch(mode){
            case "Start": //At the very beginning of the pattern
                if(currentMatch = getNextWord(pattern, position, wordBracketMatcher, spaceMatcher)){
                    if(seqMatcher.test(currentMatch)){ //Current word is like "SEQ", everything as expected
                        var match = (seqMatcher.exec(pattern))[0];
                        console.log(match); //DEBUG
                        position += match.length; //just consume this part
                        position += whiteSpaceLength(pattern, position); 
                        if(getNextChar(pattern, position, wordBracketMatcher, spaceMatcher) == "("){ //Seq has to be followed by "("
                            position += whiteSpaceLength(pattern, position);
                            position += 1;
                            position += whiteSpaceLength(pattern, position);
                            mode = "inSeq";
                        } else {
                            parserError(mode, pattern, position);
                            mode = "Finished";
                            console.log("Please add ( after SEQ!");
                        }
                        
                    }else{
                        parserError(mode, pattern, position);
                        mode="Finished";
                    }
                } else {
                    console.log("Reached unexpected end of pattern!");
                    
                    mode="Finished";
                }
                break;
            case "inSeq": //after reading SEQ(
                if(currentMatch = getNextWord(pattern, position, wordMatcher, spaceMatcher)){
                    if(varMatcher.test(currentMatch)){
                        position += whiteSpaceLength(pattern, position);
                        position += currentMatch.length;
                        position += whiteSpaceLength(pattern, position);
                        var nextChar = getNextChar(pattern, position, wordCommaBracketMatcher, spaceMatcher);
                        
                        console.log(currentMatch); //DEBUG
                        //Lookahead needed to differentiate between major or var name
                        switch(nextChar){
                            case ",": position += /[^,]*,[\n\s]*/m.exec(pattern.slice(position))[0].length; 
                                localNfa[currentMatch]={condition : [] }; //TODO: Set condition here? 
                                if(lastState){
                                    localNfa[lastState].nextState = currentMatch; //Make sure the last state remembers this state is the next one (transition from last to current)
                                    localNfa[currentMatch].previousState = lastState; //Make sure this state remembers last state as the previous one (reverting state)
                                }
                                lastState = currentMatch; //update laststate
                                
                                localNfa.size ++; //every state added increases the NFA size by one
                                break;
                            case ")": mode = "afterSeq"; 
                                position += /[^\)]*\)[\n\s]*/m.exec(pattern.slice(position))[0].length;
                                localNfa[currentMatch]={final : true, condition : [] }; //This is the last state, since the seq part ends with ")" TODO: Set condition?
                                if(lastState){
                                    localNfa[lastState].nextState = currentMatch;
                                    localNfa[currentMatch].previousState = lastState; //Make sure this state remembers last state as the previous one (reverting state)
                                }
                                lastState = currentMatch;
                                
                                localNfa.size ++;
                                break;
                            default: //match major category here
                                var currentMajorCategory = currentMatch; //if we are in a major category we have to read the var name as well
                                if(currentMatch = getNextWord(pattern, position, wordMatcher, spaceMatcher)){
                                    if(varMatcher.test(currentMatch)){
                                        position += currentMatch.length;
                                        position += whiteSpaceLength(pattern, position);
                                        //Now we can be sure we have the var name, not major, so this is a little less complicated
                                        localNfa[currentMatch]={majorCategory : currentMajorCategory, condition : [] }; //TODO: Set condition? 
                                        if(lastState){
                                            localNfa[lastState].nextState = currentMatch;
                                            localNfa[currentMatch].previousState = lastState; //Make sure this state remembers last state as the previous one (reverting state)
                                        }
                                        lastState = currentMatch;
                                        
                                        localNfa.size ++;
                                        
                                        var nextChar = getNextChar(pattern, position, wordCommaBracketMatcher, spaceMatcher);
                                        console.log(currentMatch); //DEBUG
                                        switch(nextChar){ //same thing as before, only this time the NFA state already exists
                                            case ",": position += /[^,]*,[\n\s]*/m.exec(pattern.slice(position))[0].length; 
                                                break;
                                            case ")": mode = "afterSeq"; 
                                                position += /[^\)]*\)[\n\s]*/m.exec(pattern.slice(position))[0].length;
                                                localNfa[currentMatch].final=true; //State already exists
                                                break;
                                        }
                                    }else{
                                        parserError(mode, pattern, position);
                                        mode = "Finished";
                                    }
                                } else {
                                    console.log("Reached unexp. end");
                                    mode = "Finished";
                                }
                                break;
                        }
                            
                    } else {
                        parserError(mode, pattern, position);
                        mode = "Finished";
                    }
                } else {
                    console.log("Reached unexpected end of pattern!");
                    mode = "Finished";
                }
                break;
            case "afterSeq": //read "SEQ(...)", now waiting for either "WHERE" or "T="             
                if(currentMatch = getNextWord(pattern, position, wordBracketMatcher, spaceMatcher)){
                    if(whereMatcher.test(currentMatch)){ //Found "WHERE"
                        var match = (whereMatcher.exec(pattern))[0];
                        console.log(match); //DEBUG
                        position += currentMatch.length;
                        position += whiteSpaceLength(pattern, position);
                        if(getNextChar(pattern, position, wordBracketMatcher, spaceMatcher) == "("){
                            position += whiteSpaceLength(pattern, position);
                            position += 1;
                            position += whiteSpaceLength(pattern, position);
                            mode = "inWhere";
                        } else {
                            parserError(mode, pattern, position);
                            mode = "Finished";
                            console.log("Please add ( after WHERE!");
                        }  
                        
                    }else if (tMatcher.test(currentMatch)){ //Found "T"
                        var match = (tMatcher.exec(pattern))[0];
                        console.log(match); //DEBUG
                        position += match.length;
                        position += whiteSpaceLength(pattern, position);
                        if(getNextChar(pattern, position, wordBracketEqualMatcher, spaceMatcher) == "="){
                            position += whiteSpaceLength(pattern, position);
                            position += 1;
                            position += whiteSpaceLength(pattern, position);
                            mode = "inT";
                        } else {
                            parserError(mode, pattern, position);
                            mode = "Finished";
                            console.log("Please add = after T!");
                        }
                        
                    } else { //There are no other parts ... error
                        parserError(mode, pattern, position);
                        mode="Finished";
                    }
                } else {
                    console.log("Finished parsing!");
                    mode="Finished";
                }
                break;
            case "inWhere": // found "SEQ(...)[T=...]WHERE("
                if(currentMatch = getNextWord(pattern, position, termMatcher, spaceMatcher)){
                    if(termMatcher.test(currentMatch)){ //Extract complete term, outsource this part
                        position += whiteSpaceLength(pattern, position);
                        position += currentMatch.length;
                        position += whiteSpaceLength(pattern, position);
                        nextChar = getNextChar(pattern, position, wordCommaBracketMatcher, spaceMatcher);
                        
                        //console.log("BEEEEHHHHHH!!!!!!!!!!!!!!!!" + currentMatch); //DEBUG
                        whereTranslator(lexer(currentMatch));
                        switch(nextChar){
                            case ",": position += /[^,]*,[\n\s]*/m.exec(pattern.slice(position))[0].length; break;
                            case ")": mode = "afterSeq"; 
                                position += /[^\)]*\)[\n\s]*/m.exec(pattern.slice(position))[0].length;
                                break;
                            default: 
                                break;
                        }
                            
                    } else {
                        parserError(mode, pattern, position);
                        mode = "Finished";
                    }
                } else {
                    console.log("Reached unexpected end of pattern!");
                    mode = "Finished";
                }
                break;
            case "inT": // found "SEQ(...)[WHERE(...)]T="
                if(currentMatch = getNextWord(pattern, position, numberMatcher, spaceMatcher)){
                    if(numberMatcher.test(currentMatch)){ //Only need a number now...
                        var match = (numberMatcher.exec(currentMatch))[0];
                        if(match >= localNfa.size - 1){
                            localNfa.T = match;
                        }else{
                            parserError(mode, pattern, position);
                            console.log("T has to be at least as great as the number of specified events!")
                        }
                        console.log(match); //DEBUG
                        position += match.length;
                        position += whiteSpaceLength(pattern, position);
                        mode = "Finished";
                        
                    }else{
                        parserError(mode, pattern, position);
                        mode="Finished";
                    }
                } else {
                    console.log("Reached unexpected end of pattern!");
                    
                    mode="Finished";
                }
                break;
          }
                  position += whiteSpaceLength(pattern, position);
      }while(mode != "Finished");
        console.log(localNfa); //DEBUG
        nfa.addNfa(localNfa); //Pass the NFA we created for this pattern on to the NFA handler
        console.log(nfa.savedNfas); //DEBUG
    },
    
  }

})();




var lightParse = (function () {
  
  return{
    addPattern: parser.addPattern,
    matchEvent: nfa.doStep,
    setBufferSize: nfa.setBufferSize,
    getBufferSize: nfa.getBufferSize,

  }

})()
