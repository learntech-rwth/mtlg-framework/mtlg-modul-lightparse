/**
 *  This is the lightParse module, that realizes pattern matching on event streams. Refer to the docs or wiki for a detailed description of the supported pattern description language and details on the matching technique. 
 */
var lightParse = (function () {

/**
 * Sub-module that performs the matching on automatons created by the parser.
 */
var handler = (function () {
    var automata = []; //Array that contains all automata
    var bufferedEvents;
    var buffer; //Buffer up to maxBuffer events
    var maxBuffer = 100; //max number of events buffered in buffer
    
    var timeoutTrigger = false; //This variable is set to true by the timeout function when the interval of a time-specific event has potentially ended. 
                                // This will result in special behaviour when the next event is matched (e.g. the state may be reversed)
    var timeInformation = []; //Array containing the information necessary to discern which timeout was triggered (e.g. which states do have to be reverted)
    
    var isParallelAutomaton = 1;
    var numberOfUsers = 4; //Stores the number of users for the parallel automaton

    /**
     * Sets the number of users to the parameter. 
     * This is used to implement the parallel automaton when user specific event matching is required. 
     */
    var setNumberOfUsers = function (newNumberOfUsers) {
      if(+newNumberOfUsers > 0) { //Check if number is valid
        numberOfUsers = +newNumberOfUsers; 
      } else {
        console.log("Ignoring new number of users: " + newNumberOfUsers);
      }
    }


    /**
     * Initializes newly created automata. This includes setting the currentState for all users if necessary. 
     */
    var initializeParallelAutomaton= function(newAutomaton){
        newAutomaton.currentStates = [];
        //This technique only requires isUserSpecific once, as early as possible. Delete everywhere else!
        //If the isUserSpecific attribute is true in all states and one of the conditions uses != for users no event will match
        var currentStateLabel = "Start";
        var foundUserSpec = false;
        while(newAutomaton[currentStateLabel]){
          if(!foundUserSpec && newAutomaton[currentStateLabel].isUserSpecific){
            foundUserSpec = true; 
          } else if(foundUserSpec && newAutomaton[currentStateLabel].isUserSpecific){
            newAutomaton[currentStateLabel].isUserSpecific = false;
          }
          currentStateLabel = newAutomaton[currentStateLabel].nextState;

        }
        if(foundUserSpec){ //There are user comparisons
          for(var i = 0; i < numberOfUsers; i++){
              newAutomaton.currentStates[i] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched". 
          }
          //Initiate user array index mapping
          newAutomaton.UserMapping = [];
        } else { //no need to parallize
          newAutomaton.currentStates[0] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched". 
        }
    }

    /**
     * This function checks if the provided event matches the user number, taking the mapping of the currentAutomaton into account. 
     */
    //TODO this check for users is ridiculous
    var hasCorrectUser = function(event, userNumber, currentAutomaton){
      //extract user number TODO
      var user = event.User || event.user; 

      //Check if user of event is new or old
      if(currentAutomaton.UserMapping.indexOf(user) >= 0){ //mapping contains user
        return currentAutomaton.UserMapping[userNumber] === user;
      } else if(currentAutomaton.UserMapping.length < currentAutomaton.currentStates.length ){ //enumberOfUsers){ //new user
        currentAutomaton.UserMapping.push(user);
        return currentAutomaton.UserMapping[userNumber] === user;
      } else { //No more room for user, this indicates numberOfUsers was not set correctly
        console.log("Found user with ID: " + user + ", but there have already been " + numberOfUsers + " players. This event will be ignored"); //ERROR
        return false; 
      }
      /*
        var ret = false;
        try{
            ret = event.User === userNumber || event.user === userNumber; 
        }catch (e) {
            ret = false;
        }
        return ret;
        */
    }
    
    /**
     * Deletes automaton at the specified array position. 
     */
    var deleteAutomaton = function(automatonIndex){
        if(automatonIndex !== undefined && automatonIndex !== null && automata[automatonIndex]){
            delete automata[automatonIndex];
        } else {
            console.log("Tried removing nonexisting automaton at index: " + automatonIndex);
        }
    }

    /**
     * Deletes all automata by resetting the automata array to the empty array. 
     */
    var deleteAllAutomata = function(){
      automata = []; 
    };
    
    
    /**
     * Callback function, sets the timeoutTrigger to true. 
     * This will result in a check for time-specific events in doStep when it is called next. 
     */
    var activateTimeoutTrigger = function(){
        timeoutTrigger = true; 
    }
    
    
    /**
     * Checks if currentState of automatonLabel, user is in transitive closure of firstName. 
     */
    var isInTransitiveClosure = function(automatonLabel, user, firstName) {
      if(!automata[automatonLabel][firstName]){
          //Error
          return;
      }
      
      var currentName = firstName;
      var secondName = automata[automatonLabel].currentStates[user].at;
      if(firstName === secondName){
          return true; 
      }
      while(automata[automatonLabel][currentName].nextState){
          if(automata[automatonLabel][currentName].nextState === secondName){
              return true;
          }
          currentName = automata[automatonLabel][currentName].nextState;
      }
      return false;
  }
    /**
     * Cleans up after reaching final state. Resets currentState, deletes timeInformation and deletes matched events array. 
     */
    var cleanupFinal = function(currentAutomaton, currentUser, currentState){
        //Revert state, array of matched events
        currentState.at = "Start";
        currentState.matched = {};
        //Delete all timeSpecific info from the timeInformation array
        for(timeInfo in timeInformation){
            var currentInfo = timeInformation[timeInfo];
            if(currentInfo.automatonLabel === currentAutomaton && currentInfo.user === currentUser){
                delete timeInformation[timeInfo];
            }
        }
    }
    
    
    /**
     * Tries to match event on automaton with user with the conditions of state
     */
    var matchEvent = function (event, automaton, automatonLabel, user, state){
        //console.log(automaton); //DEBUG
        //console.log(state); //DEBUG
        var currentState = automaton.currentStates[user];
        myNextStateLabel = state; //Label of next state to be matched
        myNextState = automaton[myNextStateLabel]; //Next State to be matched, contains condition
        //console.log(myNextStateLabel); //DEBUG
        //console.log(myNextState); // DEBUG
        var args = currentState.matched; //
        args[myNextStateLabel] = event;
        //console.log("args"); // DEBUG
        //console.log(args); // DEBUG
        //console.log(+user);
        //console.log(+user + 1);

        if(!myNextState.isUserSpecific || hasCorrectUser(event, user, automaton) ){
            //console.log("Correct User in matchEvent"); //DEBUG
            //set all other states to unspecific, only first event has to be correct
            /*
            var nextStateRowLabel = myNextState.nextState;
            var nextStateRow;
            while(nextStateRowLabel){
                nextStateRow = currentAutomaton[nextStateRowLabel];
                nextStateRow.isUserSpecific = false;
                nextStateRowLabel = nextStateRow.nextState;                            
            }
            */
            //now check if the array of conditions all return true
           var isConditionTrue = true; // Set to true to match events without conditions
           for (var j = 0; j < myNextState.condition.length; j++){
              isConditionTrue &= myNextState.condition[j](args);
           }
           // } && myNextState.condition(args)){ //Check for user i + 1, since user numbers start with 1? TODO
            if(isConditionTrue){ //go to next State
                //console.log("Event matched! On automaton: " + automatonLabel + " and User: " + user + " for state: " + state); //DEBUG
                currentState.at = myNextStateLabel;
                if(myNextState.final){
                    //console.log("Reached a final state"); //DEBUG
                    automaton.callback(); //TODO: Parameters? Maybe clone matched and return that
                    cleanupFinal(automatonLabel, user, currentState);
                    /* Deprecated, use cleanupFinal instead
                        currentState.at = "Start";
                        currentState.matched = {};
                    */
                } else { //No final state
                    if(myNextState.times){ //check if this event has some time constraints
                        for(pairs in myNextState.times){
                            var currentPair = myNextState.times[pairs];
                            var time = currentPair.time;
                            var partner = currentPair.partner;
                            var hasReached = false; //TEST, NOT FINAL
                            //var user =  i; //JSON.parse(JSON.stringify(i));
                            //var currentAutomaton = currentAutomatonLabel; //JSON.parse(JSON.stringify(currentAutomatonLabel));
                            var startTime = (new Date()).getTime(); 
                            var endTime = startTime + time; 
                            
                            
                            
                            //TODO
                            var currentStateLabel = automaton[currentState.at].previousState;                         
                            
                            
                            
                            
                            var timeObject = {"endTime" : endTime, "oldAt" : currentStateLabel, "automatonLabel": automatonLabel, "user" : user , "partner": partner};                            
                            
                            //Delete old timeObject if it exists
                            for(timeInfo in timeInformation){
                                var currentTimeInfo = timeInformation[timeInfo];
                                if(currentTimeInfo.oldAt === timeObject.oldAt 
                                   && currentTimeInfo.automatonLabel === timeObject.automatonLabel
                                  && currentTimeInfo.user === timeObject.user 
                                  && currentTimeInfo.partner === timeObject.partner) {
                                    delete timeInformation[timeInfo]; 
                                    console.log("Deleting old timeout"); //Debug? 
                                }
                            }
                            
                            //THEN push new timeObject
                            timeInformation.push(timeObject);
                            
                            //console.log(timeInformation); //DEBUG
                            window.setTimeout(function(){
                                activateTimeoutTrigger();
                            }, time);
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    
    return {
        savedAutomata : automata, //DEBUG
        addAutomaton : function (newAutomaton){ //add the newly created automaton to the array of automata
            if(isParallelAutomaton === 1){ //this happens only for parallel automata
                initializeParallelAutomaton(newAutomaton);
                
            }else{ //this is not supported yet
                //newAutomaton.currentStates = [];
                //newAutomaton.currentStates[0] = { at : "Start", matched : {} }; //State consists of progress in "at" and matched events in "matched". 
                
            }
            automata[automata.length] = newAutomaton;
            return automata.length -1; //return the position of the new automaton in the array to make it possible to delete it later
        },
        deleteAutomaton : deleteAutomaton,
        deleteAllAutomata : deleteAllAutomata, 
        /* Nondeterministic steps with buffering etc, unsupported 
        doStepOld : function(event){
            for(currentAutomatonLabel in automatons){ //TODO: Check for nextState == undefined and such...
                currentAutomaton = automatons[currentAutomatonLabel];
                for(currentStateLabel in currentAutomaton.currentStates){
                    currentState = currentAutomaton.currentStates[currentStateLabel];
                    myNextStateLabel = currentAutomaton[currentState.at].nextState;
                    myNextState = currentAutomaton[myNextStateLabel];
                    //DEBUG on
                    console.log("Current Automaton");
                    console.log(currentAutomaton);
                    console.log("Current State");
                    console.log(currentState);
                    console.log("Next State");
                    console.log(myNextState);
                    //DEBUG off
                    //Match event to condition:
                    if(myNextState.condition(event)){
                        //If there is still room in the buffer, we create a new state and change that
                        if(currentAutomaton.currentStates.length < maxBuffer){
                            currentAutomaton.currentStates[currentAutomaton.currentStates.length] = JSON.parse(JSON.stringify(currentState));
                            currentState = currentAutomaton.currentStates[currentAutomaton.currentStates.length - 1];
                        }
                        //Next: Save event in matched TODO
                        currentState.matched[myNextStateLabel] = event;
                        //And proceed along the transition
                        currentState.at = myNextStateLabel;
                        //Check for final state TODO HERE
                        if(currentAutomaton[myNextStateLabel].final){
                            console.log("reached final state!");
                            currentAutomaton.callback(); // TODO: Parameters? 
                        }
                    }
                }
            }
        },
        */
        /**
         * DoStep for parallel automatons, using an object for each player to store their progress
         **/
        doStep : function(event){
            
            //Handle time- specific event if timeoutTrigger is activated
            if(timeoutTrigger){
                console.log("Timeout Trigger was set") // DEBUG
                var timeNow = (new Date()).getTime();
                //check which (possibly several) timeouts have ended
                for(timeInfo in timeInformation){
                    var currentEvent = timeInformation[timeInfo];
                    if(!currentEvent){
                        continue;
                    }
                    //is the time for this event over? 
                    if(currentEvent.endTime <= timeNow){
                        //revert state if partner was not found
                        if(!isInTransitiveClosure(currentEvent.automatonLabel, currentEvent.user, currentEvent.partner)) { //TODO: Check
                            automata[currentEvent.automatonLabel].currentStates[currentEvent.user].at = currentEvent.oldAt;
                            console.log("Reverting State");
                        }
                        delete timeInformation[timeInfo];
                    }
                    
                }
                timeoutTrigger = false;
            }
            
            //Iterate through all automata and try matching the new event
            for(currentAutomatonLabel in automata){
                currentAutomaton = automata[currentAutomatonLabel];
                //Check if currentAutomaton was deleted, continue if this is the case
                if(!currentAutomaton){
                    console.log("Automaton at: " + currentAutomatonLabel + " is empty!");
                    continue;
                }
                
                //for(currentStateLabel in currentAutomaton.currentStates){
                //for(var i = 0; i < numberOfUsers; i++){ //Iterate users, current user number saved in i
                for(i in currentAutomaton.currentStates){ //TODO
                  //console.log(i);
                  var currentState = currentAutomaton.currentStates[i];
                  //console.log(currentState);
                  if(matchEvent(event, currentAutomaton, currentAutomatonLabel, +i, currentAutomaton[currentState.at].nextState) ) {
                    //Event matched next state

                  } else { //match currentState again if time relevant
                    //console.log("b"); //DEBUG
                    //console.log(currentState); //DEBUG
                    if(currentAutomaton[currentState.at].times){
                      //console.log("c"); //DEBUG
                      matchEvent(event, currentAutomaton, currentAutomatonLabel, +i, currentAutomaton.currentStates[i].at);
                    }
                  }
                }
            }
        },
        setBufferSize(newSize){ //TODO
            maxBuffer = newSize;
        },
        getBufferSize(){ //TODO
            return maxBuffer;
        },
    }
})();



/**
 * Second Sub-module of lightParse. 
 * The Parser is responsible for transforming patterns into the JavaScript representation of an automaton. 
 * This is done by calling the addPattern method. 
 */
var parser = (function f() {

  //TODO: Unnecessary? 
  var seqMatcher = /[Ss][Ee][Qq]/m; //Matches SEQ caseinsensitively allowing (unnecessarily for multiline matches). TODO: Use /i instead? 
  var tMatcher = /[Tt]/m;
  var whereMatcher = /[Ww][Hh][Ee][Rr][Ee]/m;
  var varMatcher = /[a-z][\w]*/m;
  var wordBracketMatcher = /[\w()]/m;
  var spaceMatcher = /[\s\n]/m;
  var wordMatcher = /\w/m;
  var commaMatcher = /,/m;
  var wordCommaBracketMatcher = /[\w,()]/m;
  var wordBracketEqualMatcher = /[\w=()]/m;
  var numberMatcher = /\d/m;
  var wordDotMatcher = /[\w.]/m;
  var varExtendedMatcher = /[a-z][\w.]*/m;

  //RegEx needed by the parser
  var termMatcher = /[\w.+*\/\-=><\s\n\"'!]/m; //Matches chars that can be included in a WHERE-term
  var userMatcher = /\w*[\w.]*.User/i; //Matches exactly the user variable in an event TODO
  var timeMatcher = /^\w*(.\w+)*.Time$/i; //Matches exactly the time attribute in an event TODO
  
    
  var localAutomaton; //Contains the Automaton in its current state, as it is created. Used to add conditions in whereTranslator, ...
    
    
    
  /**
    Returns an array of tokens for WHERE-terms
  **/
  var lexer = function(input){
      var ret = [];
      var currentPos = 0;
      while(currentPos < input.length){
          var currentChar = input.charAt(currentPos);
          //Check for words
          if( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') ){
              var currentVariable = "";
              while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') || (currentChar === '.') || (currentChar >= '0' && currentChar <= '9') ){
                  currentVariable += currentChar;
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
            ret[ret.length] = {
                name : "Variable",
                attribute : currentVariable,
            }
          //Check for numbers
          } else if ( currentChar >= '0' && currentChar <= '9') {
          //} else if (!isNaN(currentChar)){
              var currentNumber = 0;
              while( currentChar >= '0' && currentChar <= '9'){
              //while(!isNaN(currentChar)){
                  currentNumber *= 10;
                  currentNumber += +currentChar;
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
              ret[ret.length] = {
                name : "Number",
                attribute : currentNumber,
              }
          }
          //Check for Strings (to compare member variables to)
          else if( currentChar === '"' || currentChar === "'"){
              var currentString = "";
              var bufferCurrentChar = currentChar; //remember what kind of quotation mark was used
              currentPos++; //but don't put it into the string
              currentChar = input.charAt(currentPos);
              //while read char != opening quotation mark
              while(currentChar !== bufferCurrentChar){
                  currentString += currentChar; //add char to string
                  currentPos ++;
                  currentChar = input.charAt(currentPos);
              }
              ret[ret.length] = {
                  name : "String",
                  attribute : currentString,
              }
              currentPos ++;
          }
          //Check for opening parenthesis
          else if (currentChar === '('){
              ret[ret.length] = {
                name : "Parentheses",
                attribute : '(',
              }
              currentPos ++;
          }
          //Next check for closing parenthesis
          else if (currentChar === ')'){
              ret[ret.length] = {
                name : "Parentheses",
                attribute : ')',
              }
              currentPos ++;
          }
          //Check for arithmetic symbols
          else if (currentChar === '+' || currentChar === '*' || currentChar === '/' || currentChar === '-' ){
              ret[ret.length] = {
                  name : "ArithOperator",
                  attribute : currentChar,
              }
              currentPos ++;
          }
          //Check for comparison operators
          else if(currentChar === '>' || currentChar === '<' || currentChar === '=' || currentChar === '!'){
              var nextChar = input.charAt(currentPos + 1);
              if(currentChar === '>' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : ">",
                  }
              }
              if(currentChar === '<' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "<",
                  }
              }
              if(currentChar === '>' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : ">=",
                  }
              }
              if(currentChar === '<' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "<=",
                  }
              }
              if(currentChar === '=' && nextChar === '='){
                  currentPos ++;
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "===",
                  }
              }
              if(currentChar === '=' && nextChar !== '='){
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "===",
                  }
              }
              if(currentChar === '!' && nextChar === '='){
                  currentPos ++;
                  if(input.charAt(currentPos + 2) === '='){
                      currentPos ++;
                  }
                  ret[ret.length] = {
                      name : "CompOperator",
                      attribute : "!==",
                  }
              }
              
              currentPos ++;
          }
          //Check for the delimiting ','
          else if(currentChar === ','){
              ret[ret.length] = {
                  name : "Delimiter",
                  attribute : ",",
              }
          }
          else {
              currentPos ++;
          }
      }
      
      return ret;
      
  }
  /**
   ** Adds the corresponding function to the localAutomaton. 
   ** Checks for user specific events, adds isUserSpecific. 
   ** Checks for time-specific events and adds timeInformation. 
   **/
  var whereTranslator = function(input){
      var ret = "return ";
      var occurringVarNames = [];
      var isUserSpecific = false; //Remember if some attribute was compared to the user attribute in the event
      var isTimeSpecific = false;
      for(var i = 0; i < input.length; i++){
          var currentToken = input[i];
          var currentName = currentToken.name;
          var currentAttribute = currentToken.attribute;
          //Special Case: Variables. 
          if(currentName === "Variable"){
              //Check if this var is userSpecific
              if(userMatcher.test(currentAttribute)){
                  isUserSpecific = true; //We found a comparison to user attribute, all occurring vars will have to be set to userSpecific
              } else if(timeMatcher.test(currentAttribute)){
                  isTimeSpecific = true;
                  // All Time logic here TODO: Recursive descent or something this
                  //Get the name of the first event
                  var firstEventName = currentAttribute;
                  //make sure next symbol is '+'
                  i ++;
                  var expectedPlus = input[i];
                  var expectedPlusToken = expectedPlus.name;
                  var expectedPlusAttribute = expectedPlus.attribute;
                  //followed by a number
                  i ++;
                  var expectedNumber = input[i];
                  var expectedNumberToken = expectedNumber.name;
                  var expectedNumberAttribute = expectedNumber.attribute;
                  //followed by '<'
                  i ++;
                  var expectedGeq = input[i];
                  var expectedGeqToken = expectedGeq.name;
                  var expectedGeqAttribute = expectedGeq.attribute;
                  //lastly another time attribute
                  i ++;
                  var expectedTime = input[i];
                  var expectedTimeToken = expectedTime.name;
                  var expectedTimeAttribute = expectedTime.attribute;
                  i++;
                  if(i < input.length){
                      var expectedDelim = input[i];
                      var expectedDelimToken = expectedDelim.name;
                      var expectedDelimAttribute = expectedDelim.attribute;
                      // console.log("i is now: " + i); //DEBUG
                  } else {
                      var expectedDelimToken = "Delimiter";
                      var expectedDelimAttribute = ",";
                      // console.log("i is now: " + i + "/" + input.length); //DEBUG
                  }
                  /*
                  //DEBUG
                  console.log(expectedPlusToken);
                  console.log(expectedNumberToken);
                  console.log(expectedGeqToken);
                  console.log(expectedTimeToken);
                  console.log(expectedDelimToken);
                  console.log(expectedPlusAttribute);
                  console.log(expectedNumberAttribute);
                  console.log(expectedGeqAttribute);
                  console.log(expectedTimeAttribute);
                  console.log(expectedDelimAttribute);
                  //DEBUG
                  */
                  if(expectedPlusToken !== "ArithOperator" || expectedPlusAttribute !== '+' 
                        || expectedNumberToken !== "Number" 
                        || expectedGeqToken !== "CompOperator" //|| expectedGeqAttribute !== ">"
                        || expectedTimeToken !== "Variable" || ! timeMatcher.test(expectedTimeAttribute)
                        || expectedDelimToken !== "Delimiter" || expectedDelimAttribute !== ","){
                      //ERROR HERE
                      console.log("Error parsing where part: Time specific events have to be of a certain form. See docs for more info.");
                      throw "Invalid Syntax";
                  } else {
                      //Add information to first state, complex case with timeout
                      if(expectedGeqAttribute === ">") {
                        var firstVarName = firstEventName.split(".")[0];
                        var secondVarName = expectedTimeAttribute.split(".")[0];
                        if(!localAutomaton[firstVarName]){
                            console.log("Trying to access undefined var name '" + buffer[0] + "' in WHERE part. Aborting!");
                            throw "Undefined var in WHERE";
                        }
                        localAutomaton[firstVarName].times = localAutomaton[firstVarName].times || [];
                        localAutomaton[firstVarName].times.push({time : expectedNumberAttribute, partner : secondVarName});
                        isTimeSpecific = true; 
                      } else if (expectedGeqAttribute === "<") { //Just add the rule, easy case

                        //Create string representing first time attribute
                        var variable1 = 'args["' + currentAttribute.split(".")[0] + '"]'; //will only look for the first member
                        for (var j = 1; j < currentAttribute.split(".").length; j++){ //now we add all the other members
                            variable1 = variable1 + '["' + currentAttribute.split(".")[j] + '"]';
                        }

                        //Create string representing second time attribute
                        var variable2 = 'args["' + expectedTimeAttribute.split(".")[0] + '"]'; //will only look for the first member
                        for (var j = 1; j < expectedTimeAttribute.split(".").length; j++){ //now we add all the other members
                            variable2 = variable2 + '["' + expectedTimeAttribute.split(".")[j] + '"]';
                        }

                        //Append check for time attributes
                        ret = "try{" + variable1+ "}catch(e){console.log('Something went wrong while trying to access " + variable1+ "'); return false; }\n" + ret;
                        ret = "try{" + variable2+ "}catch(e){console.log('Something went wrong while trying to access " + variable2+ "'); return false; }\n" + ret;
                        //Create comparison
                        ret = ret + variable1 + " + " + expectedNumberAttribute + " < " + variable2;
                        console.log(ret); //DEBUG
                        //Create function and add it to condition array if possible
                        try{
                          localAutomaton[expectedTimeAttribute.split(".")[0]].condition.push(Function("args",ret));
                        } catch (e) {
                          console.log("Something went wrong trying to create a time comparison. Ignoring this part. ");
                          return;
                        }
                      }
                  }
                  //end this loop
                  continue; //TODO: Randfall irgendwas mit max int langem input würde hier wohl kaputt gehen
                  
              }
              
              //handle var names with splitting on "." 
              var buffer = currentAttribute.split(".");
              var variable = ""; //This variable will contain the final var name
              occurringVarNames[occurringVarNames.length] = buffer[0];
              ///* ONLY COMMENTED OUT FOR TESTING PURPOSES! DEBUG
              if(!localAutomaton[buffer[0]]){
                  console.log("Trying to access undefined var name '" + buffer[0] + "' in WHERE part. Aborting!");
                  throw "Undefined var in WHERE";
              }
              //*/
              variable = 'args["' + buffer[0] + '"]'; //will only look for the first member
              //ret = ret + 'args["' + buffer[0] + '"]';
              for (var j = 1; j < buffer.length; j++){ //now we add all the other members
                  variable = variable + '["' + buffer[j] + '"]';
                  //ret = ret + '["' + buffer[j] + '"]';
              }
              ret = ret + variable; //add the variable to the end of our function
              //also check if we are trying to access undefined values, which would result in errors. Make this false instead
              ret = "try{" + variable + "}catch(e){console.log('Something went wrong while trying to access " + variable + "'); return false; }\n" + ret;
              
          }
          else if (currentName === "Number"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "Parentheses"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "ArithOperator"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "CompOperator"){
              ret = ret + currentAttribute;
          }
          else if (currentName === "String"){
              ret = ret + '"' + currentAttribute + '"';
          }
          else if (currentName === "Delimiter") { //TODO: This is never executed right now (due to the way this function is called)
              
              //Handle user specific events
              if(isUserSpecific){
                  for(var m = 0; m < occurringVarNames.length; m++){
                      currentName[occurringVarNames[m]].isUserSpecific = true;
                  }
                  isUserSpecific = false;
              }
              
              //Find the correct state to add the condition to
              //by: calculating the transitive closure of all states
              //finding the var name that occurs the least in all closures?
              //or: --eliminating by finding one in the closure of the other?--
              for(var k = 0; k <= occurringVarNames.length -2; k++){
                  for(var l = k + 1; l <= occurringVarNames.length - 1; l++){
                      console.log("In for loop with k: " + k + " and l: " + l);
                        console.log(occurringVarNames);
                      if(occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[k], occurringVarNames[l])){
                          occurringVarNames.splice(k,1);
                          console.log("In case with k: " + k + " and l: " + l); //DEBUG
                            console.log(occurringVarNames); //DEBUG
                          k--;
                          break;
                      } else if (occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[l], occurringVarNames[k])){
                          occurringVarNames.splice(l,1);
                          l--;
                      }
                  }
              }
              
              //TODO: Try catch
              try{
                  //TODO: This does not work if x1.t < x2.t < x3.t we need IDs or sth. or have to add sth to ret
                /*if(isTimeSpecific){
                    localAutomaton[occurringVarNames[0]].isTimeSpecific = true; // only set the first var to is TimeSpecific
                }*/
                localAutomaton[occurringVarNames[0]].condition.push(Function("args",ret));
              }catch (e) {
                  console.log("Unable to parse where statement: \n'" + ret + "'. \nThis part will be ignored");
              }
              occurringVarNames = []; //Reset the occurring names for next clause
              console.log(ret); //DEBUG
          }
      } //end of for loop
      //save last where statement
      //TODO: Does this not require the same calculations as the non- last case? 
      /*
      var correctState = occurringVarNames[0];
      console.log(ret);
      localAutomaton[correctState].condition = Function("args",ret);
      */
      //copied from above
      //Handle user specific events
      if(isUserSpecific){
          for(var m = 0; m < occurringVarNames.length; m++){
              localAutomaton[occurringVarNames[m]].isUserSpecific = true;
          }
          isUserSpecific = false;
      }
      
      //Return if time specific (ret is empty)
      if(isTimeSpecific){
          isTimeSpecific = false; //Unnecessary
          return; 
      }
      
      //Find the correct state to add the condition to
      //by: calculating the transitive closure of all states
      //finding the var name that occurs the least in all closures?
      //or: --eliminating by finding one in the closure of the other?--
      for(var k = 0; k <= occurringVarNames.length -2; k++){
          for(var l = k + 1; l <= occurringVarNames.length - 1; l++){
              console.log("In for loop with k: " + k + " and l: " + l); //DEBUG
              console.log(occurringVarNames); //DEBUG
              if(occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[k], occurringVarNames[l])){
                  occurringVarNames.splice(k,1);
                  console.log("In case with k: " + k + " and l: " + l); //DEBUG
                    console.log(occurringVarNames); //DEBUG
                  k--;
                  break;
              } else if (occurringVarNames[k] !== occurringVarNames[l] && isInTransitiveClosure(occurringVarNames[l], occurringVarNames[k])){
                  occurringVarNames.splice(l,1);
                  l--;
              }
          }
      }

      //TODO: Try catch
      try{
        localAutomaton[occurringVarNames[0]].condition.push(Function("args",ret));
      }catch (e) {
          console.log("Unable to parse where statement: \n'" + ret + "'. \nThis part will be ignored");
      }
      console.log(ret); //DEBUG
  }
  /*
   * This function returns true if secondName is in the transitive closure of firstName
   */
  var isInTransitiveClosure = function(firstName, secondName) {
      //Retrun false if the firstName state doesn't exist
      if(!localAutomaton[firstName]){
          //Error
          return;
      }
      //Return true if the names are equal
      if(firstName === secondName) {
        return true; 
      }
      var currentName = firstName;
      while(localAutomaton[currentName].nextState){
          if(localAutomaton[currentName].nextState === secondName){
              return true;
          }
          currentName = localAutomaton[currentName].nextState;
      }
      return false;
  }

  
  
  /**
    Get the next word according to patMatch in text starting from position, ignore all patIgnore before patMatch
    Deprecated
  **/
  var getNextWord = function (text, position, patMatch, patIgnore) {
      var ret = "";
      var inWord = false;
      var i = position;
      
      while (i < text.length && patIgnore.test(text.charAt(i))) {
          i++;
      }
      while (i < text.length && patMatch.test(text.charAt(i))){
          inWord = true;
          ret += text.charAt(i);
          i++;
      }
      if(inWord){
          return ret;
      }else{
          return undefined; // TODO
      }
      
  }
  
  /**
    Get the next char according to patMatch in text starting from position, ignore all patIgnore before patMatch
    Deprecated
  **/
  var getNextChar = function (text, position, patMatch, patIgnore){
      var ret = "";
      var inWord = false;
      var i = position + 0;
      
      while(i < text.length && patIgnore.test(text.charAt(i))){
          i++;
      }
      if(i < text.length && patMatch.test(text.charAt(i))){
          inWord = true;
          ret += text.charAt(i);
          i++;
      }
      if(inWord){
          return ret;
      }else{
          return undefined; // TODO
      }
  }


  var localInput = ""; //local version of input pattern
  var localCurrentPos = 0; //current position in input

  /**
   * Sets the localInput to input and resets the localCurrentPos to 0
   **/
  var initializeLocalInput= function (input){
    localInput = input;
    localCurrentPos = 0;
  }

  /**
   * Returns the next word in localInput from localCurrentPos
   **/
  var ruleGetNextWord = function(){
    var buffer = detectWord(localCurrentPos);
    localCurrentPos = buffer.pos;
    return buffer.returnObject;
  }

  /**
   * Returns the next WHERE-term starting from localCurrentPos. 
   */
  var ruleGetNextWherePart = function(){
    var ret = "";
    while(localCurrentPos < localInput.length){
      var currentChar = localInput.charAt(localCurrentPos);
      if(termMatcher.test(currentChar)){
        ret += currentChar; 
        localCurrentPos++;
      } else if (currentChar === "("){ //Opening parenthesis found, start special routine
        console.log("Entering `(` part"); //DEBUG
        ret += currentChar; 
        localCurrentPos ++;
        ret += ruleGetNextWherePart(); //Search until closing bracket was found
        currentChar = localInput.charAt(localCurrentPos); //Append found word
        if( currentChar !== ')' ) {
          throw "Not enough closing brackets in WHERE part! Aborting this pattern";
        } else {
          ret += currentChar; 
          localCurrentPos ++;
        }
        /*if( (currentChar !== ")" && currentChar !== "," ) || localInput.charAt(localCurrentPos -1) !== ")"){ //Look at first char after word, if it is not a closing bracket or comma something probably went wrong.
          throw "Not enough closing brackets in WHERE part! Aborting this pattern";
        } else { //seems to be OK, continue
          ret += currentChar; 
          localCurrentPos ++;
          if(currentChar === ",") { //or if currentChar is the endinig comma return the found pattern
            return ret; 
          }
        } */
      } else if (currentChar === ")" || currentChar === ","){
        //ret += currentChar; 
        //localCurrentPos++;
        console.log(ret); //DEBUG
        return ret; 
      } else {
        throw "Parser Error: Unknown symbol: " + currentChar;
      }



    }
    throw "Unexpected End of Where part! Aborting this patter: " + ret;
    return "";
  };


  /**
   * Used by ruleGetNextWord and rulePeekNextWord to find the next word. 
   * Returns word and new position in an object. 
   */
  var detectWord = function (position) {
    var currentPos = position; 
    while(currentPos < localInput.length){
      var currentChar = localInput.charAt(currentPos);
      //Check for words
      if( (currentChar >= 'a' && currentChar <= 'z') ) { //Vars
        var currentWord = "";
        while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') || (currentChar >= '0' && currentChar <= '9') ) {
          currentWord += currentChar; 
          currentPos ++;
          currentChar = localInput.charAt(currentPos);
        }
        return {pos : currentPos, returnObject : {name: "Variable", 
                attribute: currentWord,
        },
        };
      } else if( (currentChar >= 'A' && currentChar <= 'Z') ){ //Control commands
        var currentWord = "";
        while( (currentChar >= 'a' && currentChar <= 'z') || (currentChar >= 'A' && currentChar <= 'Z') ){
          currentWord += currentChar; 
          currentPos ++;
          currentChar = localInput.charAt(currentPos);
        }
        switch (true) {
          case /^seq$/i.test(currentWord): 
            return {pos: currentPos, returnObject: {name: "SEQ", 
                      attribute: "",
            },
            };
            break; 
          case /^where$/i.test(currentWord):
            return {pos: currentPos, returnObject : {name: "WHERE", 
                      attribute: "",
            },
            };
            break;
          case /^users$/i.test(currentWord):
            return {pos: currentPos, returnObject : {name: "USERS",
                      attribute: "",
            },
            };
            break;
          default: 
            //throw "Unknown command sequence: " + currentWord; 
            return {pos: currentPos, returnObject : {name: "ERROR", 
              attribute: currentWord,
            },
            };
        }
      //Check for parentheses
      } else if (currentChar === '(' || currentChar === ')' ) {
        currentPos++;
        return {pos: currentPos, returnObject : {name: "Parentheses",
                  attribute: currentChar
        },
        };
        currentPos ++;
      } else if (currentChar === ',') {
        currentPos++;
        return {pos: currentPos, returnObject : {name: "Delimiter", 
          attribute: ',',
        },
        };
      //Remove spaces etc
      } else if (currentChar >= '0' && currentChar <= '9'){
        var currentWord = 0;
        while (currentChar >= '0' && currentChar <= '9') {
          currentWord *= 10;
          currentWord += +currentChar; 
          currentPos ++;
          currentChar = localInput.charAt(currentPos);
        }
        return {pos : currentPos, returnObject : {name: "Number", 
                attribute: currentWord,
        },
        };
      } else {
        currentPos ++; 
      }
    }

    return {pos: currentPos, returnObject : {name: "END",
            attribute: "",
    },
    };

  }

  var rulePeekNextWord = function(){
    var currentPos = localCurrentPos; 
    return detectWord(currentPos).returnObject; 
  }
  
  /**
    Returns the number of white spaces after position in text
  **/
  var whiteSpaceLength = function (text, position){
      var ret = 0;
      var i = position;
      while(i < text.length && /[\s\n]/m.test(text.charAt(i))){
          ret ++;
          i++;
      }
      return ret;
  }
  
  /**
    Create and output Generic Error message to console
  **/
  var parserError = function (mode, pattern, position){
    pattern = pattern || localInput; 
    position = position || localCurrentPos; //TODO: Might be one too far
      console.log("Error parsing " + mode + " part in \n'" 
            + pattern + "'\nat position: " + position
            +" (char: " + pattern.charAt(position) + ").");
  }
  
    

  var testBasicSeq = function() {
    var buffer = parser.addPattern("SEQ(x1,x2,x3,x4,x5)");
    if(
      localAutomaton.size !== 6 // Start + plus states
      || !localAutomaton.x1 //States have to exist
      || !localAutomaton.x2 //States have to exist
      || !localAutomaton.x3 //States have to exist
      || !localAutomaton.x4 //States have to exist
      || !localAutomaton.x5 //States have to exist
      || !localAutomaton.Start //Check Start state

    ){
        console.log("Basic Sequence Test failed: Not enough states created!");
        lightParse.deleteAutomaton(buffer);
        return false;
    }
    lightParse.deleteAutomaton(buffer);

    //Check if it is possible to create states with names that are also used as keywords
    buffer = parser.addPattern("SEQ(start,currentState,user,time,nextState, at, matched)");
    if(
      localAutomaton.size !== 8 // Start + plus states
      || !localAutomaton.start //States have to exist
      || !localAutomaton.currentState //States have to exist
      || !localAutomaton.user //States have to exist
      || !localAutomaton.time //States have to exist
      || !localAutomaton.nextState //States have to exist
      || !localAutomaton.at //States have to exist
      || !localAutomaton.matched //States have to exist
      || !localAutomaton.Start //Check Start state
      //TODO: more tests if these are in fact the states and not sth. els. also: TODO make a matching test with these names 

    ){
        console.log("Basic Sequence Test failed: Not enough states created!");
        lightParse.deleteAutomaton(buffer);
        return false;
    }


    lightParse.deleteAutomaton(buffer);
    return true; 
  }

  var testWrongSeq = function(){
    var buffer = parser.addPattern("SEQ(A1, x2, x3)");
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with upper case accepted!");
      return false; 
    }
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern("SEQ(x1, x2, A3)");
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with upper case accepted!");
      return false; 
    }
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern("SEQ(1x, x2, x3)");
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with number accepted!");
      return false; 
    }
    buffer = parser.addPattern("SEQ(x1, x2, 3x)");
    lightParse.deleteAutomaton(buffer);
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with number accepted!");
      return false; 
    }
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern("SEQ(x1, x1, x3)");
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with dubble naming accepted!");
      return false; 
    }
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern("SEQ(x1, x2, x2)");
    if (buffer !== -1) {
      console.log("Wrong Sequence Test failed: State with dubble naming accepted!");
      return false; 
    }

    lightParse.deleteAutomaton(buffer);
    return true; 
  }


  testBasicWhere = function(){
    var buffer = parser.addPattern("SEQ(x1,x2,x3,x4) WHERE(x1.a = 1, x1.b = 'abc', x2.a = 1, x2.b = 'abc', x3.a = 1, x3.b = 'abc', x4.a = 1, x4.b = 'abc')");
    if(
      localAutomaton.x1.condition.length !== 2
      || localAutomaton.x2.condition.length !== 2
      || localAutomaton.x3.condition.length !== 2
      || localAutomaton.x4.condition.length !== 2
    ){
      console.log("Basic Where Test failed: Wrong number of conditions created!");
      return false;
    }
    var matched = { x1 : {a : 1, b : 1}};
    var doesMatch = true;
    for(cond in localAutomaton.x1.condition){
      if(!localAutomaton.x1.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(doesMatch){
      console.log("Basic Where Test failed: Condition function returned true where it shouldn't!");
      return false;
    }
    matched = { x1 : {a : 1, b : 'abc'}};
    doesMatch = true;
    for(cond in localAutomaton.x1.condition){
      if(!localAutomaton.x1.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(!doesMatch){
      console.log("Basic Where Test failed: Condition function returned false where it shouldn't!");
      return false;
    }

    matched = { x2 : {a : 1, b : 1}};
    doesMatch = true;
    for(cond in localAutomaton.x2.condition){
      if(!localAutomaton.x2.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(doesMatch){
      console.log("Basic Where Test failed: Condition function returned true where it shouldn't!");
      return false;
    }
    matched = { x2 : {a : 1, b : 'abc'}};
    doesMatch = true;
    for(cond in localAutomaton.x2.condition){
      if(!localAutomaton.x2.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(!doesMatch){
      console.log("Basic Where Test failed: Condition function returned false where it shouldn't!");
      return false;
    }

    matched = { x3 : {a : 1, b : 1}};
    doesMatch = true;
    for(cond in localAutomaton.x3.condition){
      if(!localAutomaton.x3.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(doesMatch){
      console.log("Basic Where Test failed: Condition function returned true where it shouldn't!");
      return false;
    }

    matched = { x3 : {a : 1, b : 'abc'}};
    doesMatch = true;
    for(cond in localAutomaton.x3.condition){
      if(!localAutomaton.x3.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(!doesMatch){
      console.log("Basic Where Test failed: Condition function returned false where it shouldn't!");
      return false;
    }

    matched = { x4 : {a : 1, b : 1}};
    doesMatch = true;
    for(cond in localAutomaton.x4.condition){
      if(!localAutomaton.x4.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(doesMatch){
      console.log("Basic Where Test failed: Condition function returned true where it shouldn't!");
      return false;
    }
    matched = { x4 : {a : 1, b : 'abc'}};
    doesMatch = true;
    for(cond in localAutomaton.x4.condition){
      if(!localAutomaton.x4.condition[cond](matched)){
        doesMatch = false;
      }
    }
    if(!doesMatch){
      console.log("Basic Where Test failed: Condition function returned false where it shouldn't!");
      return false;
    }


    lightParse.deleteAutomaton(buffer);
    return true; 
  }

  testParenthesesWhere = function () {
    var pattern1 = "SEQ(x1,x2) WHERE(((x1.a + 20) * ((37 - 13) / 2)) / 21 >= 13)";
    var buffer = parser.addPattern(pattern1);
    var matched = { x1 : {a : 1}};
    var doesMatch = true;
    if(buffer === -1){ //addPattern failed
      console.log("Parentheses Test failed: Correct pattern was not added: " + pattern1);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    if(!localAutomaton.x1.condition.length === 1 || localAutomaton.x1.condition[0](matched)){
      console.log("Parentheses Test failed: Translation of pattern failed: " + pattern1);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    matched = { x1 : {a : 2}};
    if(localAutomaton.x1.condition.length !== 1 || localAutomaton.x1.condition[0](matched)){
      console.log("Parentheses Test failed: Translation of pattern failed: " + pattern1);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    lightParse.deleteAutomaton(buffer);
    var pattern2 = "SEQ(x1,x2) WHERE(((x1.a + 20) * ((37 - 13)) / 2)) / 21 >= 13)";
    buffer = parser.addPattern(pattern2);
    if(buffer !== -1 && localAutomaton.x1.condition.length >= 1) {
      console.log("Parentheses Test failed: Incorrect pattern was added: " + pattern2);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    
    lightParse.deleteAutomaton(buffer);
    return true;
  }

  testUserWhere = function () {
    var pattern1 = "SEQ(x1,x2,x3,x4) WHERE (x1.a = 1, x1.user = x2.user, x1.b = 2, x2.user = x3.user, x3.user != x4.user)";
    var buffer = parser.addPattern(pattern1);
    if(buffer === -1 || !localAutomaton.x1.isUserSpecific){ //other states?
      console.log("User Where Test failed: Correct pattern was not added as expected: " + pattern1);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    var pattern2 = "SEQ(x1,x2,x3,x4) WHERE (x1.a = 1, x2.user = x1.user, x1.b = 2, x2.user = x3.user, x3.user != x4.user)";
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern(pattern2);
    if(buffer === -1 || !localAutomaton.x1.isUserSpecific){ //other states?
      console.log("User Where Test failed: Correct pattern was not added as expected: " + pattern2);
      lightParse.deleteAutomaton(buffer);
      return false;
    }
    var pattern3 = "SEQ(x1,x2,x3,x4) WHERE (x2.user != x1.user, x2.user = x3.user, x3.user != x4.user)";
    lightParse.deleteAutomaton(buffer);
    buffer = parser.addPattern(pattern2);
    if(buffer === -1 || !localAutomaton.x1.isUserSpecific){ //other states?
      console.log("User Where Test failed: Correct pattern was not added as expected: " + pattern2);
      lightParse.deleteAutomaton(buffer);
      return false;
    }

    lightParse.deleteAutomaton(buffer);
    return true;
  }

  testTimeWhere = function () {
    var pattern1 = "SEQ(x1,x2,x3,x4) WHERE (x1.a = 1, x1.time + 1000 > x2.time, x1.b = 2, x2.time + 1000 > x3.time, x3.time + 1000 <  x4.time)";
    var buffer = parser.addPattern(pattern1);
    if(buffer === -1 || !localAutomaton.x1.times || !localAutomaton.x2.times){ //
      console.log("Time Where Test failed: Correct pattern was not added as expected: " + pattern1);
      return false;
    }
  if(localAutomaton.x1.times[0].time !== 1000 || localAutomaton.x1.times[0].partner !== "x2" 
    || localAutomaton.x2.times[0].time !== 1000 || localAutomaton.x2.times[0].partner !== "x3"
    || localAutomaton.x4.condition.length< 1 ){
    console.log("Time Where Test failed: Wrong time information created in times array for pattern: " + pattern1);
    lightParse.deleteAutomaton(buffer);
    return false;
  }
  //TODO: Test wrong pattern? 

    lightParse.deleteAutomaton(buffer);
    return true;
  }





  return{
    //Test- Suite
    testLexer : lexer, 
    testTrans : whereTranslator,
    testBasicSequ : testBasicSeq,
    testWrongSequ : testWrongSeq,
    testBasicWh : testBasicWhere,
    testParanthWhere : testParenthesesWhere,
    testUserWh : testUserWhere,
    testTimeWh : testTimeWhere,


      /**
        This function takes a pattern and callback function as arguments and creates, as well as registers an Automaton that matches the given pattern. 
      **/
    addPattern: function (pattern, callbackFunction){
        var mode = "Start";//parsing mode: Start -> in SEQ -> AfterSEQ -> inWhile -> afterSeq -> inT -> Finished
        var position = 0; //position in pattern
        var patternCpy = JSON.parse(JSON.stringify(pattern)); //create a copy to make sure the original is preserved (for error handling)
        var currentMatch; // current word as received by the "Lexer"
        callbackFunction = callbackFunction || function () {};
        localAutomaton = {Start : {condition : []}, size : 1, callback : callbackFunction, }; //The Automaton that will be given to the Automaton handler in the end TODO: Is the condition is start necessary? 
        initializeLocalInput(pattern); //set localInput to pattern and localCurrentPos to 0
        var lastState = "Start"; //Remember the last state for transitions
      do{ //This do loop will go through the pattern and parse the next word according to the current mode
        console.log(mode); //DEBUG
        switch(mode){
            case "Start": //At the very beginning of the pattern
              currentMatch = ruleGetNextWord();
              if(currentMatch.name !== "SEQ"){
                parserError(mode);
                mode = "Finished";
                localAutomaton = null;
              } else {
                currentMatch = ruleGetNextWord();
                if(currentMatch.name !== "Parentheses" || currentMatch.attribute !== "("){
                  parserError(mode);
                  mode = "Finished";
                  localAutomaton = null;
                } else {
                  console.log("Found SEQ("); //DEBUG
                  mode = "inSeq";
                  console.log(currentMatch); //DEBUG
                  console.log(rulePeekNextWord()); //DEBUG
                }
              }
                break;
            case "inSeq": //after reading SEQ(
              currentMatch = ruleGetNextWord();
              console.log(currentMatch); //DEBUG
              if(currentMatch.name !== "Variable"){
                console.log(currentMatch); //DEBUG
                if(currentMatch.name === "Parentheses" && currentMatch.attribute === ')'){
                  console.log("Ending SEQ- part"); //DEBUG
                  mode = "afterSeq";
                } else if (currentMatch.name === "Delimiter" && currentMatch.attribute === ',') {
                  //Consume delimiters, next var name is added in next loop round
                }else {
                  parserError(mode);
                  mode = "Finished";
                  localAutomaton = null;
                }
              } else {
                if(localAutomaton[currentMatch.attribute]) { //State already exists, double naming
                  parserError(mode);
                  mode = "Finished";
                  localAutomaton = null; 
                } else { //create new state, name is valid
                  localAutomaton[currentMatch.attribute]={condition : [] }; //TODO: Set condition here? 
                  if(lastState) { //remember previous and next state
                    localAutomaton[lastState].nextState = currentMatch.attribute;
                    localAutomaton[currentMatch.attribute].previousState = lastState; 
                  }
                  lastState = currentMatch.attribute; //update last state info
                  var bufferWord = rulePeekNextWord(); //Look-ahead: is this the last variable? Set final state then
                  if(bufferWord.name === "Parentheses" && bufferWord.attribute === ')'){
                    localAutomaton[currentMatch.attribute].final = true; 
                  }
                  localAutomaton.size ++;
                }
              }
              break;
            case "afterSeq": //read "SEQ(...)", now waiting for either "WHERE" or "T="             
            currentMatch = ruleGetNextWord();
            switch(currentMatch.name){
              case "WHERE": 
                var bufferWord = ruleGetNextWord();
                if(bufferWord.name !== "Parentheses" || bufferWord.attribute !== '('){
                  parserError(mode); //WHERE part starts with (
                  mode = "Finished";
                  localAutomaton = null;
                } else {
                  mode = "inWhere"; 
                }
                break;
                //USERS nos supported
              /*case "USERS": 
                if(ruleGetNextWord().name !== "comparisonOperator"){ //TODO
                  parserError(mode); //WHERE part starts with (
                  mode = "Finished";
                } else {
                  mode = "Finished"; //TODO
                }
                break;
              */
              case "END": 
                console.log("Reached end of pattern. Automaton created succesfully");
                mode = "Finished";
                break;
              default: 
                console.log("Encountered unknown control sequence. Aborting"); //TODO
                mode = "Finished";
                localAutomaton = null;
            }
            break;
            case "inWhere": // found "SEQ(...)[T=...]WHERE("
              console.log("Entering inWhere"); //DEBUG
              try{
                currentMatch = ruleGetNextWherePart(); //Returns the term without ending , or )
              }
              catch (error){
                console.log("Error while reading WHERE- term: " + error);
                console.log("Ignoring this part");
                currentMatch = null;
              }
              if(currentMatch){
                console.log("Translating inWhere"); //DEBUG
                whereTranslator(lexer(currentMatch));
                console.log("Finished Translating  inWhere"); //DEBUG
              }

              var bufferWord = ruleGetNextWord(); 
              if (bufferWord.name === "Delimiter"){
                //Consume Delimiter
              } else if (bufferWord.name === "Parentheses") {
                mode = "afterSeq";
              } else if (bufferWord.name === "END") { //Problems while creating last WHERE term (missing closing bracket?)
                mode = "Finished";
              }
              break;
          }
      }while(mode != "Finished");
        if(!localAutomaton) {
          console.log("Failed to create automaton"); 
          return -1; 
        }
        console.log(localAutomaton); //DEBUG
        var automatonNumber = handler.addAutomaton(localAutomaton); //Pass the Automaton we created for this pattern on to the Automaton handler
        console.log(handler.savedAutomata); //DEBUG
        return automatonNumber;
    },
    
  }

})();


var testBasicMatching = function () {
  //Create and register pattern
  var wasMatched = false;
  var pattern1 = "SEQ(x1,x2) WHERE(x1.a = 1, x1.b = 2, x2.a = 2, x2.b = 1)";
  var buffer = lightParse.addPattern(pattern1, function(){
    wasMatched = true; 
  });
  //Create Event stream
  var event1 = {a : 1, b : 1};
  var event2 = {a : 1, b : 2};
  var event3 = {a : 2, b : 1};
  var event4 = {a : 2, b : 2};
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match final
  //sleep(10); //Unnecassary? 
  if(!wasMatched){
    console.log("Basic Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched = false;
  lightParse.matchEvent(event4); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match final
  if(!wasMatched){
    console.log("Basic Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched = false;
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event4); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched){
    console.log("Basic Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match final
  if(!wasMatched){
    console.log("Basic Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched = false;

  return true; 

}

var testParallelMatching = function () {
  //Create and register patterns
  //Pattern 1
  var wasMatched1 = false;
  var pattern1 = "SEQ(x1,x2) WHERE(x1.a = 1, x1.b = 2, x2.a = 1, x2.b = 2)";
  var buffer1 = lightParse.addPattern(pattern1, function(){
    wasMatched1 = true;
  });

  //Pattern 2
  var wasMatched2 = false;
  var pattern2 = "SEQ(x1,x2) WHERE(x1.a = 2, x1.b = 1, x2.a = 2, x2.b = 1)";
  var buffer2 = lightParse.addPattern(pattern2, function(){
    wasMatched2 = true; 
  });
  //Create Event stream
  var event1 = {a : 1, b : 1};
  var event2 = {a : 1, b : 2};
  var event3 = {a : 2, b : 1};
  var event4 = {a : 2, b : 2};
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event1); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match pat1
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match pat2
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event4); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match pat1 final
  if(!wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched1 = false;
  
  lightParse.matchEvent(event3); //Should match pat2 final
  if(!wasMatched2 || wasMatched1){
    console.log("Parallel Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched2 = false;

  lightParse.matchEvent(event1); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match pat1
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event4); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event2); //Should match pat1 final
  if(!wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  wasMatched1 = false;

  lightParse.matchEvent(event1); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match pat2 
  if(wasMatched1 || wasMatched2){
    console.log("Parallel Matching Test failed: Pattern not found.");
    return false;
  }
  lightParse.matchEvent(event3); //Should match pat2 final
  if(wasMatched1 || !wasMatched2){
    console.log("Parallel Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched1 = false;

  return true; 

}

var testUserMatching = function () {
  //TODO: Set users to four
  //Create pattern
  //Pattern 1
  var pattern1 = "SEQ(x1,x2,x3) WHERE(x1.user = x2.user, x1.user != x3.user, x1.a = 1, x2.a = 1)";
  var wasMatched1 = false;
  var buffer1 = lightParse.addPattern(pattern1, function () {
    wasMatched1 = true;
  });
  //Pattern 2
  var pattern2 = "SEQ(x1,x2,x3) WHERE(x1.user = x2.user, x1.user != x3.user, x1.a = 2, x2.a = 2)";
  var wasMatched2 = false;
  var buffer2 = lightParse.addPattern(pattern2, function () {
    wasMatched2 = true;
  });

  //Create Events
  var event11 = {a : 1, user : 1};
  var event12 = {a : 1, user : 2};
  var event13 = {a : 1, user : 3};
  var event14 = {a : 1, user : 4};

  var event21 = {a : 2, user : 1};
  var event22 = {a : 2, user : 2};
  var event23 = {a : 2, user : 3};
  var event24 = {a : 2, user : 4};

  //Test matching
  lightParse.matchEvent(event11); //Should match pat1
  console.log(event11);
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event11); //Should match pat1
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event11); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event12); //Should match pat1 final
  if(!wasMatched1 || wasMatched2){
    console.log(wasMatched1);
    console.log(wasMatched2);
    console.log("User Matching Test failed: Pattern1 not found or Pattern2  where there was none.");
    return false;
  }
  wasMatched1 = false;
  lightParse.matchEvent(event13); //Should match pat1
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event22); //Should match pat2
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event12); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event22); //Should match pat2
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event22); //Should not match
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event23); //Should match pat1 and pat2 final
  if(!wasMatched1 || !wasMatched2){
    console.log("User Matching Test failed: Pattern not found.");
    return false;
  }
  wasMatched1 = false;
  wasMatched2 = false;
  lightParse.matchEvent(event13); //Should match pat1
  if(wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  lightParse.matchEvent(event12); //Should match pat1 final
  if(!wasMatched1 || wasMatched2){
    console.log("User Matching Test failed: Pattern found where there was none.");
    return false;
  }
  wasMatched1 = false;

  return true;

}

var testTimeMatching = function () {
  var wasSuccessfull = true; 
  //TODO: Set users to four
  //Create pattern
  //Pattern 1
  var pattern1 = "SEQ(x1,x2,x3,x4) WHERE (x1.a = 1, x1.time + 1000 > x2.time, x2.a = 3, x2.time + 1000 > x3.time, x3.time + 1000 <  x4.time)";
  var wasMatched1 = false;
  var buffer1 = lightParse.addPattern(pattern1, function () {
    wasMatched1 = true;
  });
  //Pattern 2
  var pattern2 = "SEQ(x1,x2) WHERE (x1.a = 2, x1.time + 1000 > x2.time)";
  var wasMatched2 = false;
  var buffer2 = lightParse.addPattern(pattern2, function () {
    wasMatched2 = true;
  });

  //Create Events
  var event11 = {a : 1, time : 1};
  var event12 = {a : 1, time : 10000};
  var event13 = {a : 3, time : 10000};

  var event21 = {a : 2, time : 1};
  var event22 = {a : 2, time : 10000};

  //Test matching
  
  //Set up timeouts for events
  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
  }, 1);

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 100);

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
  }, 200);

  window.setTimeout(function () {
    lightParse.matchEvent(event22); //Should not match pat2 any longer
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 1300);
    

  //Start test- run for second pattern: match first 3, see if it is possible to match x4 early

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 2500);

  window.setTimeout(function () {
    lightParse.matchEvent(event13); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 2600);

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2 
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 2700);

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should not match pat2 yet
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 2700);

  window.setTimeout(function () {
    lightParse.matchEvent(event12); //Should match pat2 now
    if(!wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 4000);
  


  //Start test- run for second pattern: match first state twice, check if it is reverted
  

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 5000);

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2: x1 and prolong
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 5800);

  window.setTimeout(function () {
    lightParse.matchEvent(event13); //Should match pat2 
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 6600);

  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern found where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 6700);

  window.setTimeout(function () {
    lightParse.matchEvent(event12); //Should match pat2 now
    if(!wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 7700);
  


  //Start parallel test


  window.setTimeout(function () {

    console.log("Time Matching Test: Commencing Parallel Test"); //Announce test in console

    lightParse.matchEvent(event11); //Should match pat1
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9000);

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9100);
 
  window.setTimeout(function () {
    lightParse.matchEvent(event13); //Should match pat1 and pat2 final
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9200);
  

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2 and pat 1
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9300);

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2 final
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9400);
 
  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match none
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9500);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event12); //Should match pat1 final
    if(!wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9600);

  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9700);
 
  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat1 x1 and pat2 final
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9800);
 
  window.setTimeout(function () {
    lightParse.matchEvent(event13); //Should match pat1 x2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 9900);
 
  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2 x1 and pat1 x3
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 10000);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat2 final
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 10100);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event12); //Should match pat1 final
    if(!wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
    console.log("Time Matching Text: Finished Parallel Test");
  }, 10200);
  
  //Test what happens if one is reverted while the other is still going
  
  window.setTimeout(function () {
    console.log("Time Matching Text: Commencing Revert Test");
    lightParse.matchEvent(event11); //Should match pat1 x1
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 10300);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event21); //Should match pat2 x1
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 11000);
  
  //Reverting pat1
  
  window.setTimeout(function () {
    lightParse.matchEvent(event22); //Should match pat2 final
    if(wasMatched1 || !wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 11500);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat1 x1 again
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 11600);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event13); //Should match pat1 x2
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 11700);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event11); //Should match pat1 x3
    if(wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
  }, 11800);
  
  window.setTimeout(function () {
    lightParse.matchEvent(event12); //Should match pat1 final
    if(!wasMatched1 || wasMatched2){
      console.log("Time Matching Test failed: Pattern1 not found or Pattern2 where there was none.");
      wasSuccessfull = false; 
    } else {
      console.log("Time Matching Test: Positive progess");
    }
    wasMatched1 = wasMatched2 = false;
    console.log("Time Matching Text: Finished Revert Test");
  }, 12800);
  
  
}

var testParser = function (){
  return lightParse.testBasicSequence() && lightParse.testWrongSequence() && lightParse.testBasicWhere() && lightParse.testParenthesesWhere() && lightParse.testUserWhere() && lightParse.testTimeWhere();
}

var testHandler= function(){
  return lightParse.testBasicMatching() && lightParse.testParallelMatching() && lightParse.testUserMatching(); // && testTimeMatching(); 
}
  
  return{
    addPattern: parser.addPattern,
    matchEvent: handler.doStep,
    deleteAutomaton : handler.deleteAutomaton, 
    deleteAllAutomata : handler.deleteAllAutomata, 
    setNumberOfUsers : handler.setNumberOfUsers, 
    testBasicSequence : parser.testBasicSequ,
    testWrongSequence : parser.testWrongSequ,
    testBasicWhere : parser.testBasicWh,
    testParenthesesWhere : parser.testParanthWhere,
    testUserWhere : parser.testUserWh,
    testTimeWhere : parser.testTimeWh,
    testBasicMatching : testBasicMatching,
    testParallelMatching : testParallelMatching,
    testUserMatching : testUserMatching,
    testTimeMatching : testTimeMatching,
    testParser : testParser, 
    testHandler : testHandler, 
    mySpeedTest : function () {
      console.log((new Date).getTime());
      var buffer1 = lightParse.addPattern("SEQ(x1,x2,x3) WHERE (x1.a = 1, x2.b = 'abc', x3.c != 7)", function(){/*console.log("Matched 1")*/});
      var buffer2 = lightParse.addPattern("SEQ(x1,x2,x3) WHERE (x1.user = x2.user, x2.b = 'abc', x3.c != 7)", function(){console.log("Matched 2")});
      var event = [{a : 1, b : 'abc', c : 2, user : 10}, {a : 1, b : 'abc', c : 2, user : 101}, {a : 1, b : 'abc', c : 2, user : 991}, {a : 1, b : 'abc', c : 2, user : 321}]; 
      console.log((new Date).getTime());
      for(var i = 0; i < 10; i ++){
        lightParse.matchEvent(event[i % 4]);
      }
      console.log((new Date).getTime());
      lightParse.deleteAutomaton(buffer1);
      lightParse.deleteAutomaton(buffer2);
    },


  }

})()
